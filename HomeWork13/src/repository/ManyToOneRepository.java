package repository;

import java.util.List;

public interface ManyToOneRepository<T, E> {
    List<E> findAllSecondsByFirst(T t);
    T findOneFirstBySecond(E e);
    void setSecondByFirst(T t, List<E> e);
    void setFirstBySecond(E e, T t);
}
