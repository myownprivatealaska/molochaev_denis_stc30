package repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CourseAndLesson implements ManyToOneRepository<Long, String> {
    private String filename;

    public CourseAndLesson(String filename) {
        this.filename = filename;
    }

    @Override
    public List<String> findAllSecondsByFirst(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            List<String> result = new ArrayList<>();
            String current = bufferedReader.readLine();
            while (current != null) {
                String[] date = current.split(";");
                if (Long.parseLong(date[0]) == id) result.add(date[1]);
                current = bufferedReader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public Long findOneFirstBySecond(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String current = bufferedReader.readLine();
            while (current != null) {
                String[] date = current.split(";");
                if ((date[1]).equals(name)) return Long.parseLong(date[0]);
                current = bufferedReader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public void setSecondByFirst(Long id, List<String> names) {
        List<String> fromFile = findAllSecondsByFirst(id);
        for (String name : names) {
            if (name != null && !fromFile.contains(name)) {
                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                    bufferedWriter.append(id + ";" + name + "\r\n");
                    bufferedWriter.close();
                } catch (IOException e) {
                    throw new IllegalArgumentException("Файл не найден!");
                }
            }
        }
    }

    @Override
    public void setFirstBySecond(String name, Long id) {
        Long idFromFile = findOneFirstBySecond(name);
        if (idFromFile == null) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                bufferedWriter.append(id + ";" + name + "\r\n");
                bufferedWriter.close();
            } catch (IOException e) {
                throw new IllegalArgumentException("Файл не найден!");
            }
        } else if (!idFromFile.equals(id)) System.err.println("Этот урок уже используется на другом курсе!");
    }
}
