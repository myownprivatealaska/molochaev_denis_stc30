package repository;

import model.Course;
import model.Lesson;
import util.Converter;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LessonRepositoryImpl implements LessonRepository {
    private String filename;
    private CourseAndLesson courseAndLesson;

    public LessonRepositoryImpl(String filename, CourseAndLesson courseAndLesson) {
        this.filename = filename;
        this.courseAndLesson = courseAndLesson;
    }

    Converter<String, Lesson> getLessonByString = string -> {
        String data[] = string.split(";");
        Long id = courseAndLesson.findOneFirstBySecond(data[0]);
        return new Lesson(data[0], LocalDateTime.parse(data[1]), new Course(id));
    };

    Converter<Lesson, String> getStringByLesson = lesson ->
            lesson.getName() + ";" + lesson.getDate_of_event() + "\n";

    @Override
    public Lesson findByName(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String currentLine = bufferedReader.readLine();
            while (currentLine != null) {
                Lesson lesson = getLessonByString.convert(currentLine);
                if (lesson.getName().equals(name)) return lesson;
                currentLine =  bufferedReader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public void save(Lesson entity) {
        Lesson candidate = findByName(entity.getName());
        if (candidate == null) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                bufferedWriter.append(getStringByLesson.convert(entity));
                bufferedWriter.close();
                if (entity.getCourse()!= null)
                courseAndLesson.setFirstBySecond(entity.getName(),
                        entity.getCourse().getId());
            } catch (IOException e) {
                throw new IllegalArgumentException("Файл не найден");
            }
        }
    }

    @Override
    public List<Lesson> findAll() {
        try {
            List<Lesson> lessonsList = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String currentLine = bufferedReader.readLine();
            while (currentLine != null) {
               Lesson lesson = getLessonByString.convert(currentLine);
                lessonsList.add(lesson);
                currentLine = bufferedReader.readLine();
            }
            bufferedReader.close();
            return lessonsList;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public void update(Lesson entity) {

    }
}
