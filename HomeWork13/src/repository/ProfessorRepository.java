package repository;

import model.Professor;

public interface ProfessorRepository extends Repository<Professor> {
    Professor findByLastName(String last_name);
}
