package repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CourseAndProfessor implements ManyToManyRepository<Long, String> {
    private String filename;

    public CourseAndProfessor(String filename) {
        this.filename = filename;
    }

    @Override
    public List<String> listOfSecondByFirst(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            List<String> result = new ArrayList<>();
            String current = bufferedReader.readLine();
            while (current != null && current.length()>1) {
                String[] date = current.split(";");
                if (Long.parseLong(date[0]) == id && date[1].length()>0) result.add(date[1]);
                current = bufferedReader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public List<Long> listOfFirstBySecond(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            List<Long> result = new ArrayList<>();
            String current = bufferedReader.readLine();
            while (current != null) {
                String[] date = current.split(";");
                if (date[0].equals(name) && date[1].length()>0) result.add(Long.parseLong(date[1]));
                current = bufferedReader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    @Override
    public void setListSecondByFirst(Long id, List<String> names) {
        List<String> namesFromFile = listOfSecondByFirst(id);
        for (String name: names) {
            if (name!=null && !namesFromFile.contains(name)) {
                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                    String string = id + ";" + name + "\r\n";
                    bufferedWriter.write(string);
                    bufferedWriter.close();
                } catch (IOException e) {
                    throw new IllegalArgumentException("Файл не найден!");
                }
            }
        }
    }

    @Override
    public void setListFirstBySecond(String name, List<Long> ids) {
        List<Long> listIds = listOfFirstBySecond(name);
        for (Long id: ids) {
            if (id!=null && listIds.contains(id)) {
                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                    bufferedWriter.append(id + ";" + name + "\r\n");
                } catch (IOException e) {
                    throw new IllegalArgumentException("Файл не найден!");
                }
            }
        }
    }
}
