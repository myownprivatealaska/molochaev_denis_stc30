package repository;

import model.Course;

import java.util.Optional;

public interface CourseRepository extends Repository<Course> {
    Course findCourseByCourseName(String courseName);
    Optional<Course> findCourseByCourseId(Long courseId);
}
