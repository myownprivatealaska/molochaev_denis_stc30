package repository;

import model.Lesson;

public interface LessonRepository extends Repository<Lesson> {
    Lesson findByName(String name);
}
