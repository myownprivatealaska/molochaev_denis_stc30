package repository;

import model.Course;
import model.Lesson;
import model.Professor;
import util.Converter;
import util.IdGeneratorInFile;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CourseRepositoryImpl implements CourseRepository {
    private String filename;
    private IdGeneratorInFile idGeneratorInFile;
    private CourseAndProfessor courseAndProfessor;
    private CourseAndLesson courseAndLesson;

    public CourseRepositoryImpl(String filename, IdGeneratorInFile idGeneratorInFile, CourseAndProfessor courseAndProfessor, CourseAndLesson courseAndLesson) {
        this.filename = filename;
        this.idGeneratorInFile = idGeneratorInFile;
        this.courseAndProfessor = courseAndProfessor;
        this.courseAndLesson = courseAndLesson;
    }


    private Converter<String, Course> getCourseByString = string -> {
        String data[] = string.split(";");
        List<Professor> professors = new ArrayList<>();
        List<String> professorsNameList = courseAndProfessor.listOfSecondByFirst(Long.parseLong(data[0]));
        List<String> lessonsNameList = courseAndLesson.findAllSecondsByFirst(Long.parseLong(data[0]));
        for (String name : professorsNameList) {
            if (name != null) professors.add(new Professor(name));
        }
        List<Lesson> lessons = new ArrayList<>();
        for (String name : lessonsNameList) {
            if (name != null) lessons.add(new Lesson(name));
        }
        return new Course(Long.parseLong(data[0]), data[1],
                LocalDateTime.parse(data[2]), LocalDateTime.parse(data[3]), professors, lessons);
    };

    private Converter<Course, String> getStringByCourse = course -> course.getId() + ";" +
            course.getName() + ";" +
            course.getDate_from() + ";" +
            course.getDate_to() + ";" + "\n";


    @Override
    public Course findCourseByCourseName(String courseName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = getCourseByString.convert(current);
                if (course.getName().equals(courseName)) return course;
                current = bufferedReader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден");
        }
    }

    @Override
    public Optional<Course> findCourseByCourseId(Long courseId) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = getCourseByString.convert(current);
                if (course.getId().equals(courseId)) return Optional.of(course);
                current = bufferedReader.readLine();
            }
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден");
        }
    }

    @Override
    public List<Course> findAll() {
        List<Course> result = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = getCourseByString.convert(current);
                if (course != null) result.add(course);
                current = bufferedReader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден");
        }
    }

    @Override
    public void save(Course entity) {
        Course candidate = findCourseByCourseName(entity.getName());
        if (candidate == null) {
            entity.setId(idGeneratorInFile.nextId());
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                bufferedWriter.write(getStringByCourse.convert(entity));
                bufferedWriter.close();
                if (entity.getProfessors() != null) {
                    courseAndProfessor.setListSecondByFirst(entity.getId(),
                            entity.getProfessors()
                                    .stream().map(prof -> prof.getSecond_name())
                                    .collect(Collectors.toList()));
                }
                if (entity.getLessons() != null) {
                    courseAndLesson.setSecondByFirst(entity.getId(),
                            entity.getLessons()
                                    .stream().map(lesson -> lesson.getName())
                                    .collect(Collectors.toList()));
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Файл не найден");
            }
        }
    }

    @Override
    public void update(Course entity) {

    }
}
