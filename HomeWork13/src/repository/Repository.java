package repository;

import java.util.List;

/**
 *  JPA для бедных, определяем почти все операции crud-а, в имплементации и потомках
 *  определим поведение и специализацию в виде поиска конкретной сущности;
 * @param <T>
 */
public interface Repository<T> {
    List<T> findAll();
    void save(T entity);
    void update(T entity);
}
