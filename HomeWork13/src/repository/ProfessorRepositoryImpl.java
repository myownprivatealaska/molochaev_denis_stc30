package repository;

import model.Course;
import model.Professor;
import util.Converter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProfessorRepositoryImpl implements ProfessorRepository {
    private CourseAndProfessor courseAndProfessor;
    private String filename;

    public ProfessorRepositoryImpl(String filename, CourseAndProfessor courseAndProfessor) {
        this.courseAndProfessor = courseAndProfessor;
        this.filename = filename;
    }

    private Converter<String, Professor> getProfessorByString = string -> {
        String data[] = string.split(";");
        List<Course> courses = new ArrayList<>();
        List<Long> listCoursesIds = courseAndProfessor.listOfFirstBySecond(data[1]);
        for (Long id : listCoursesIds) {
            if (id != null) courses.add(new Course(id));
        }
        return new Professor(data[0], data[1], Integer.parseInt(data[2]), courses);
    };

    private Converter<Professor, String> getStringByProfessor = professor ->
            professor.getFirst_name() + ";" +
                    professor.getSecond_name()  + ";" +
                    professor.getExperience() + ";" + "\n";

    @Override
    public Professor findByLastName(String last_name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String currentLine = bufferedReader.readLine();
            while (currentLine != null && currentLine.length()>1) {
                Professor professor = getProfessorByString.convert(currentLine);
                if (professor.getSecond_name().equals(last_name)) return professor;
                currentLine =  bufferedReader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    /**
     * Идем по-хардкору, фактически save это обобщение, когда пользователя нет и когда он есть,
     * но мы делаем его апдейт
     *
     * @param entity
     */
    @Override
    public void save(Professor entity) {
        Professor candidate = findByLastName(entity.getSecond_name());
        if (candidate == null) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true));
                bufferedWriter.write(getStringByProfessor.convert(entity));
                bufferedWriter.close();
                if (entity.getCourses() != null)
                    courseAndProfessor.setListFirstBySecond(entity.getSecond_name(),
                            entity.getCourses().stream().map(course -> course.getId()).collect(Collectors.toList()));
            } catch (IOException e) {
                throw new IllegalArgumentException("Файл не найден");
            }
        }
    }

    @Override
    public List<Professor> findAll() {
        try {
            List<Professor> professorsList = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String currentLine = bufferedReader.readLine();
            while (currentLine != null) {
                Professor professor = getProfessorByString.convert(currentLine);
                professorsList.add(professor);
                currentLine = bufferedReader.readLine();
            }
            bufferedReader.close();
            return professorsList;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }

    /**
     * Очень грубо допустим(!), что фамилия преподавателя является уникальной и будет для нас PK
     * естественно делать надо не так и PK должен быть id, но такого условия не было
     *
     * @param entity
     */
    @Override
    public void update(Professor entity) {

    }
}
