package repository;

import java.util.List;

public interface ManyToManyRepository<F, S> {
    List<S> listOfSecondByFirst(F f);

    List<F> listOfFirstBySecond(S s);

    void setListSecondByFirst(F f, List<S> s);

    void setListFirstBySecond(S s, List<F> f);
}
