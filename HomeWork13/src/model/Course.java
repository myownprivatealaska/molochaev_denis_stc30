package model;

import java.time.LocalDateTime;
import java.util.List;

public class Course {
    Long id;
    String name;

    public LocalDateTime getDate_from() {
        return date_from;
    }

    public LocalDateTime getDate_to() {
        return date_to;
    }

    LocalDateTime date_from;
    LocalDateTime date_to;
    List<Professor> professors;
    List<Lesson> lessons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Professor> getProfessors() {
        return professors;
    }

    public void setProfessors(List<Professor> professors) {
        this.professors = professors;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Course(String name, List<Professor> professors) {
        this.name = name;
        this.professors = professors;
        setDefaultTime();
    }


    public Course(String name) {
        this.name = name;
        setDefaultTime();
    }

    public Course(Long id) {
        this.id = id;
    }

    public void setDefaultTime() {
        this.date_from = LocalDateTime.now();
        this.date_to = LocalDateTime.now().plusDays(10);
    }

    public Course(Long id, String name, LocalDateTime date_from,
                  LocalDateTime date_to, List<Professor> professors, List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.date_from = date_from;
        this.date_to = date_to;
        this.professors = professors;
        this.lessons = lessons;
    }

    public Course(String name, List<Professor> professors, List<Lesson> lessons) {
        this.name = name;
        this.professors = professors;
        this.lessons = lessons;
        setDefaultTime();
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date_from=" + date_from +
                ", date_to=" + date_to + "\n\r" +
                ", professors=" + professors + "\n\r" +
                ", lessons=" + lessons +
                '}';
    }
}
