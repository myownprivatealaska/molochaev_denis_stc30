package model;

import java.util.List;
import java.util.Objects;


public class Professor {
    String first_name;

    String second_name;
    int experience;
    List<Course> courses;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Professor() {
    }

    public Professor(String first_name, String second_name, int experience) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.experience = experience;
    }

    public Professor(String second_name) {
        this.second_name = second_name;
    }

    public Professor(String first_name, String second_name, int experience, List<Course> courses) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.experience = experience;
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Professor professor = (Professor) o;
        return experience == professor.experience &&
                Objects.equals(first_name, professor.first_name) &&
                Objects.equals(second_name, professor.second_name) &&
                Objects.equals(courses, professor.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first_name, second_name, experience, courses);
    }

    @Override
    public String toString() {
        return "Professor{" +
                "first_name='" + first_name + '\'' +
                ", second_name='" + second_name + '\'' +
                ", experience=" + experience +
                ", courses=" + courses +
                '}';
    }
}
