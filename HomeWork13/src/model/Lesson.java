package model;

import java.time.LocalDateTime;
import java.util.Date;

public class Lesson {
    String name;
    LocalDateTime date_of_event;
    Course course;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDate_of_event() {
        return date_of_event;
    }

    public void setDate_of_event(LocalDateTime date_of_event) {
        this.date_of_event = date_of_event;
    }

    public Course getCourse() {
        return course;
    }

    public Lesson() {
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Lesson(String name) {
        this.name = name;
        this.date_of_event = LocalDateTime.now();
    }

    public Lesson(String name, LocalDateTime date_of_event, Course course) {
        this.name = name;
        this.date_of_event = date_of_event;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "name='" + name + '\'' +
                ", date_of_event=" + date_of_event +
                ", course=" + course +
                '}';
    }
}
