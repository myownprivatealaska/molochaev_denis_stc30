package util;

import model.Course;
import model.Lesson;
import model.Professor;
import repository.CourseRepository;
import repository.LessonRepository;
import repository.ProfessorRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Все это большая условность, необходимая для реализации конкретной задачи.
 * Целью прокси слоя является конвертация полученных из персистентного слоя raw-объектов
 * в их финальное представление, иными словами наполнение коллекций настоящими объектами вместо их PK
 */
public class ProxyLayer {
    private CourseRepository courseRepository;
    private ProfessorRepository professorRepository;
    private LessonRepository lessonRepository;

    public ProxyLayer(CourseRepository courseRepository,
                      ProfessorRepository professorRepository,
                      LessonRepository lessonRepository) {
        this.courseRepository = courseRepository;
        this.professorRepository = professorRepository;
        this.lessonRepository = lessonRepository;
    }

    Proxy<Professor> professorProxy = professor -> {
        List<Course> resultCourse = new ArrayList<>();
        for (Course course:professor.getCourses()) {
            Optional<Course> realCourse = courseRepository.findCourseByCourseId(course.getId());
            if (realCourse.isPresent()) resultCourse.add(realCourse.get());}

        return new Professor(professor.getFirst_name(), professor.getSecond_name(),
                professor.getExperience(), resultCourse);
    };

    Proxy<Lesson> lessonProxy = lesson -> {
        Optional<Course> realCourse = courseRepository.findCourseByCourseId(lesson.getCourse().getId());
        if (realCourse.isPresent()) return new Lesson(lesson.getName(), lesson.getDate_of_event(), realCourse.get());
        return lesson;
    };

    Proxy<Course> courseProxy = course -> {
        List<Lesson> resultLesson =  new ArrayList<>();
        List<Professor> resultProfessor =  new ArrayList<>();
        for (Lesson lesson:course.getLessons()) {
            Lesson entity = lessonRepository.findByName(lesson.getName());
            if (entity!=null) resultLesson.add(entity);
        }
        for (Professor professor:course.getProfessors()) {
            Professor entity = professorRepository.findByLastName(professor.getSecond_name());
            if (entity!=null) resultProfessor.add(entity);
        }
        return new Course(course.getId(), course.getName(),
                course.getDate_from(), course.getDate_to(), resultProfessor, resultLesson);
    };

    /**
     * Методы для получения реальных объектов на основе raw-объектов из репозитория
     * @param professor
     * @return
     */

    public Professor getRealByProxy(Professor professor) {
        return professorProxy.getRealByProxy(professor);
    }
    public Course getRealByProxy(Course course) {
        return courseProxy.getRealByProxy(course);
    }
    public Lesson getRealByProxy(Lesson lesson) {
        return lessonProxy.getRealByProxy(lesson);
    }
}
