package util;

import java.io.*;

public class IdGeneratorInFile implements IdGenerator
{
    private String filename;

    public IdGeneratorInFile(String filename) {
        this.filename = filename;
    }

    @Override
    public Long nextId() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String counter = bufferedReader.readLine();
            if (counter == null) counter = "0";
            Long id = Long.parseLong(counter);
            id++;
            bufferedReader.close();
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename));
            bufferedWriter.write(id.toString());
            bufferedWriter.close();
            return id;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл для генератора не найден!");
        }
    }
}
