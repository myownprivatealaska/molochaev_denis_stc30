package util;

public interface Proxy<T> {
    T getRealByProxy(T t);
}
