import model.Course;
import model.Lesson;
import model.Professor;
import repository.*;
import util.IdGeneratorInFile;
import util.ProxyLayer;

import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        /**
         * Начинаем с того, что создаем репозитории связей ManyToOne, ManyToMany
         */
        CourseAndLesson courseAndLesson = new CourseAndLesson("course-lesson.txt");
        CourseAndProfessor courseAndProfessor = new CourseAndProfessor("course-professor.txt");
        /**
         * Далее на основе существующих компонентов создаем репозитории моделей
         */

        LessonRepository lessonRepository = new LessonRepositoryImpl("lesson.txt", courseAndLesson);
        IdGeneratorInFile idGeneratorInFile = new IdGeneratorInFile("sequence.txt");
        CourseRepository courseRepository = new CourseRepositoryImpl("course.txt", idGeneratorInFile,
                courseAndProfessor, courseAndLesson);
        ProfessorRepository professorRepository = new ProfessorRepositoryImpl("professor.txt", courseAndProfessor);

        Lesson english = new Lesson("Английский");
        Lesson chemical = new Lesson("Химия");
        lessonRepository.save(english);
        lessonRepository.save(chemical);


        Professor ivanov = new Professor("Иванов", "Никита", 3);
        Professor petrov = new Professor("Петров", "Петр", 1);
        professorRepository.save(ivanov);
        professorRepository.save(petrov);


        Course course = new Course("Языки мира", Collections.singletonList(ivanov),
                Collections.singletonList(english));
        courseRepository.save(course);
        Course course1 = new Course("Топоры мира", Collections.singletonList(petrov),
                Collections.singletonList(chemical));
        courseRepository.save(course1);

        /**
         * Вот тут самый смак, объекты их файлов приходят в сыром виде, т.к. вместо
         * всех коллекций PK объектов
         */
        ProxyLayer proxyLayer = new ProxyLayer(courseRepository,
                professorRepository,
                lessonRepository);
        List<Course> courses = courseRepository.findAll();
        for (Course entity:courses) {
            System.out.println();
            System.out.println("------------------------------------------------");
            System.out.println(entity);
            System.out.println("-------------После проксирования----------------");
            System.out.println(proxyLayer.getRealByProxy(entity));
            System.out.println("------------------------------------------------");
            System.out.println();
        }

        List<Professor> professors = professorRepository.findAll();
        for (Professor entity:professors) {
            System.out.println();
            System.out.println("------------------------------------------------");
            System.out.println(entity);
            System.out.println("-------------После проксирования----------------");
            System.out.println(proxyLayer.getRealByProxy(entity));
            System.out.println("------------------------------------------------");
            System.out.println();
        }

        List<Lesson> lessons = lessonRepository.findAll();
        for (Lesson entity:lessons) {
            System.out.println();
            System.out.println("------------------------------------------------");
            System.out.println(entity);
            System.out.println("-------------После проксирования----------------");
            System.out.println(proxyLayer.getRealByProxy(entity));
            System.out.println("------------------------------------------------");
            System.out.println();
        }

    }
}
