import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    /**
     * Для реализации сортировки воспользуемся разбиением Хаора
     */
    public static void quicksort(String[] array) {
        quicksort(array, 0, array.length - 1);
    }
    public static void quicksort(String[] array, int lo, int hi) {
        if (lo >= hi) return;
        int p = splitter(array, lo, hi); // по принципу хаора p должно быть окончательным
        quicksort(array, lo, p - 1); // ни один элемент тут не должен быть больше p
        quicksort(array, p + 1, hi); // ни один элемент тут не должен быть меньше p
    }

    public static int splitter(String[] array, int lo, int hi) {
        int i = lo;
        int j = hi + 1;
        String central = array[lo];
        while (i < j) {
            while (array[++i].compareToIgnoreCase(central) < 0) if (i == hi) break;
            while (array[--j].compareToIgnoreCase(central) > 0) if (j == lo) break; // в конечном итоге j будет в позиции, где array[j] < target, туда мы и помести lo
            if (i < j) swap(array, i, j);
            else break;
        }
        swap(array, lo, j);
        return j;
    }

    /**
     * Вспомогательная функция обмена, избегаем дубля
     */
    public static void swap(String[] array, int i, int j) {
        String temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void main(String[] args) {
        try {
            List<String> inputList = Files.lines(Paths.get("text.txt")).collect(Collectors.toList());
            String[] input = inputList.toArray(new String[0]);
            quicksort(input);
            System.out.println(Arrays.asList(input));
        } catch (IOException e) {
            throw new IllegalArgumentException("file not found exception");
        }
    }
}
