public class User {
    private String firstName;
    private String lastName;
    private boolean isWorker;
    private int age;

    private User() { }

    public static class builder {
        private User builderUser;

        public builder(){
            this.builderUser = new User();
        }

        public builder firstName(String name) {
            builderUser.firstName = name;
            return this;
        }
        public builder lastName(String name) {
            builderUser.lastName = name;
            return this;
        }
        public builder age(int age) {
            builderUser.age = age;
            return this;
        }
        public builder isWorker(boolean isWorker) {
            builderUser.isWorker = isWorker;
            return this;
        }
        public User build() {
            return builderUser;
        }
    }
    public void showUserEntity() {
        System.out.println("Имя: " + firstName + ", фамилия: " + lastName + ", возраст: " + age + ", работает: " + isWorker);
    }
}