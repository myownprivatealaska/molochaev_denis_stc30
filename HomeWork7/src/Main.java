public class Main {

    public static void main(String[] args) {
        User user = new User.builder()
                .firstName("Данила")
                .lastName("Козловский")
                .age(37)
                .isWorker(false)
                .build();
        user.showUserEntity();
    }
}
