import java.util.Arrays;
import java.util.Scanner;

class Program1 {
    public static void main(String[] args) {
        int[] arrayFirst = {1, 4, 3, 2, 5, 6, 203};
        System.out.println("Задание 1: ");
        System.out.println("Исходный массив: " + Arrays.toString(arrayFirst));
        System.out.println("Сумма элементов массива равна: " + getSumArray(arrayFirst));

        System.out.println("Задание 2: ");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размер массива: ");
        int n = scanner.nextInt();
        int[] arraySecnod = new int[n];
        for (int i = 0; i < arraySecnod.length; i ++) {
            System.out.print("Введите элемент номер " + i + ": ");
            arraySecnod[i] = scanner.nextInt();
        }
        revertArray(arraySecnod);

        System.out.println("Задание 3: ");
        System.out.println("Среднее арифметическое массива: " + Arrays.toString(arraySecnod) + " равно " + avrArray(arraySecnod));

        System.out.println("Задание 4: ");
        moveMaxMin(arraySecnod);


        System.out.println("Задание 5: ");
        bubbleSort(arraySecnod);

        System.out.println("Задание 6: ");
        long array2number = arrayToNumber(arrayFirst);
        System.out.println("Массив: " + Arrays.toString(arrayFirst) + " в виде числа выглядит как: " + array2number);
    }
    //Имплементации:
    //Первое задание
    static int getSumArray(int[] array) {
        int arraySum = 0;
        for (int i=0; i<array.length; i++) {
            arraySum += array[i];
        }
        return arraySum;
    }
    //Второе задание
    static void revertArray(int[] array) {
        int pooledValue = 0;
        System.out.println("Исходный массив имеет вид " + Arrays.toString(array));
        for (int i=0; i<array.length/2; i++) {
            pooledValue = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = pooledValue;
        }
        System.out.println("Развернутый массив " + Arrays.toString(array));
    }
    //Третье задание
    static int avrArray(int[] array) {
        int arrayAverage = 0;
        for (int i=0; i<array.length; i++) {
            arrayAverage += array[i];
        }
        arrayAverage /= array.length;
        return arrayAverage;
    }
    //Четвертое задание
    static void moveMaxMin(int[] array) {
        int indexOfMin = 0;
        int indexOfMax = 0;
        System.out.println("Исходный массив: " + Arrays.toString(array));

        int maxVal = array[0];
        int minVal = array[0];
        for (int i = 1; i < array.length; i++) {
            if (minVal > array[i]) {
                minVal = array[i];
                indexOfMin = i;
            }
            if (maxVal < array[i]) {
                maxVal = array[i];
                indexOfMax = i;
            }
        }
        array[indexOfMin] = maxVal;
        array[indexOfMax] = minVal;
        System.out.println("Массив с поменянными местами максимальным и минимальным элементом: " + Arrays.toString(array));
    }
    static void bubbleSort(int[] array) {
        //Пятое задание
        int pooledVal = 0;
        System.out.println("Исходный массив - " + Arrays.toString(array));
        for (int i = array.length - 1; i > 0 ; i--) {
            for (int j = 0; j < i; j ++) {
                if (array[j] < array[j+1]) {
                    pooledVal = array[j];
                    array[j] = array [j+1];
                    array[j+1]=pooledVal;
                }
            }
        }
        System.out.println("Вид после сортировки: "+ Arrays.toString(array));
    }

    //Шестое задание
    static long arrayToNumber(int[] array) {
        long number = 0l;
        int power = 1;
        int indexRunner = array.length - 1;
        while(indexRunner >= 0) {
            if (array[indexRunner] > 9) {
                int i;
                for (i = 1; i <= array[indexRunner];) {
                    power *= 10;
                    i*=10;
                }
                number += (long)array[indexRunner]*(power/i);
            }
            else {
                number += (long) array[indexRunner] * power;
                power *= 10;
            }
            indexRunner--;
        }
        return number;
    }

}
