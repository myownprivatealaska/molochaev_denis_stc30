class Program2 {
    public static void main(String[] args) {
        double a = 1.2;
        double b = 2;
        //Интервал остается прежним, смотрим разное количество четных отрезков
        int[] ns = {2, 4, 6, 8, 10};
        for (int i = 0; i < ns.length; i++) {
            System.out.println("Результат вычисления определенного интеграла методом Симпсона для n=" + ns[i] +
                    " равен: " + simpsonIntegralSolution(a,b,ns[i]));
        }
    }
    public static double func(double x) {
        return x * x;
    }

    public static double simpsonIntegralSolution(double a, double b, int n) {
        double result = 0;
        double h = (b - a) / n;
        //Делаем массив даблов где индекс - количество узлов(узлов на один больше четных отрезков), а значение X
        double[] iterator = new double[n+1];
        for (int i = 0; i < iterator.length; i++) {
            iterator[i] = a + h * i;
        }
        /**Считаем integral f(x)dx методом Сипсона, учитывая общую формулу f(x0)+f(x2)+4(f(x1))
         * в случае 2 отрезков, в случае увеличесения числа отрезков добавляем обсчет для
         * границ остается прежний, а промежуточные четные узлы суммируются и умнажаюются на 2, нечетные на 4
         */
        double odd = 0;
        double even = 0;
        //Интегрируем по промежуточным узлам
        for (int i = 1; i < iterator.length - 1; i++) {
            if (i % 2 == 0) even += func(iterator[i]);
            else odd += func(iterator[i]);
        }
        //Собираем в кучу по формуле, включая частный случай из примера
        result = (h/3)*(func(iterator[0]) + func(iterator[n]) + (even * 2) + (odd * 4));
        return result;
    }
}
