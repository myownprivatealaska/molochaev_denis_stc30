package model;

import java.time.LocalDateTime;

public class Shot {
    private Integer id;
    private LocalDateTime date;
    boolean isHit;
    private Game game;
    private Player attack;
    private Player defending;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isHit() {
        return isHit;
    }

    public void setHit(boolean hit) {
        this.isHit = hit;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getAttack() {
        return attack;
    }

    public void setAttack(Player attack) {
        this.attack = attack;
    }

    public Player getDefending() {
        return defending;
    }

    public void setDefending(Player defending) {
        this.defending = defending;
    }

    public Shot(LocalDateTime date, boolean isHit, Game game, Player attack, Player defending) {
        this.date = date;
        this.isHit = isHit;
        this.game = game;
        this.attack = attack;
        this.defending = defending;
    }
}
