package server;

import service.GameService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {
    private final static String SUCCESSFULLY_MSG = "You are connected and assigned an id: ";
    private final static String START_MSG = "The game has started, your opponent: ";
    private final static String FINAL = "STOP GAME IT'S OVER, THE WINNER IS ";


    private Socket firstPlayerSocket;
    private Socket secondPlayerSocket;

    private Integer game_id;

    private final GameService gameService;

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    private PlayerThread firstClient;
    private PlayerThread secondClient;

    private boolean isGameStarted = false;

    private final String TRY_SHOOT = "BANG";

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println();
            System.out.println("*****************************************************");
            System.out.println("Server started successfully at host " + serverSocket.getInetAddress().getHostAddress()
                    + " and port " + port);
            System.out.println("*****************************************************");
            while (!isGameStarted) {
                if (firstPlayerSocket == null) {
                    firstPlayerSocket = serverSocket.accept();
                    System.out.println("First client connected!");

                    firstClient = new PlayerThread(firstPlayerSocket);
                    firstClient.playerValue = "Client#1";
                    firstClient.ip = firstPlayerSocket.getInetAddress().getHostAddress();
                    firstClient.outgoing.println(SUCCESSFULLY_MSG + firstClient.playerValue);
                    firstClient.start();
                } else {
                    secondPlayerSocket = serverSocket.accept();
                    System.out.println("Second client connected!");

                    secondClient = new PlayerThread(secondPlayerSocket);
                    secondClient.playerValue = "Client#2";
                    secondClient.ip = secondPlayerSocket.getInetAddress().getHostAddress();
                    secondClient.outgoing.println(SUCCESSFULLY_MSG + secondClient.playerValue);
                    secondClient.start();

                    isGameStarted = true;
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Класс вспомогательных процессов
     */
    private class PlayerThread extends Thread {
        private Socket player;
        private String namePlayer;
        private String ip;
        private String playerValue;

        private BufferedReader incoming;
        private PrintWriter outgoing;

        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.incoming = new BufferedReader(new InputStreamReader(player.getInputStream()));
                this.outgoing = new PrintWriter(player.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            while (!firstPlayerSocket.isClosed() || !secondPlayerSocket.isClosed()) {
                try {
                    String rawMessage = incoming.readLine();
                    String incomingMessage = playerValue + " " + rawMessage;
                    if (rawMessage != null) {
                        System.out.println("Received a message from " + player.getInetAddress().getHostAddress() +
                                ":" + player.getPort() + " --> " + incomingMessage);
                        String[] command = incomingMessage.split(" ");
                        // Логика начала игры: сигнал вида "Client% *START* %USERNAME%"
                        switch (command[1]) {
                            case "START":
                                if (command.length > 2) {
                                    this.namePlayer = command[2];
                                    if (firstClient.namePlayer != null &&
                                            secondClient != null && secondClient.namePlayer != null) {
                                        game_id = gameService.startGame(firstClient.namePlayer, firstClient.ip,
                                                secondClient.namePlayer, secondClient.ip);

                                        firstClient.outgoing.println(START_MSG + secondClient.namePlayer);
                                        secondClient.outgoing.println(START_MSG + firstClient.namePlayer);
                                    } else
                                        firstClient.outgoing.println(firstClient.namePlayer + ", please wait for your opponent!");
                                }
                                break;
                            case "MOVE":
                                if (command[0].equals("Client#1")) secondClient.outgoing.println(rawMessage);
                                else firstClient.outgoing.println(rawMessage);
                                break;
                            /**
                             * Логика стрельбы и прочего формата "Client% *COMMAND* *ADDITIONAL_INFO*"
                             * Где возможны следующие варианты, например если стрелял Client#1:
                             * Client#1 SHOT_T - попытка выстрела, информируем второе сообщение с результатом,
                             * это сообщение-информирование для второго игрока
                             * Client#1 SHOT_R HIT - выстрел с результатом попадание
                             * Client#1 SHOT_R MISSED - выстрел с результатом промах
                             * Последние два сообщения уже фиксируем, время выстрела вычисляем так,
                             * время выстрела - время анимации которое необходимо для дистежние линии с врагом
                             */
                            case "SHOT_T":
                                if (command[0].equals("Client#1")) {
                                    secondClient.outgoing.println(TRY_SHOOT);
                                } else firstClient.outgoing.println(TRY_SHOOT);
                                break;

                            case "SHOT_R":
                                Boolean result = command[2].equals("HIT");
                                if (command[0].equals("Client#1")) {
                                    gameService.makeShot(game_id, firstClient.namePlayer, secondClient.namePlayer, result);
                                } else if (command[0].equals("Client#2")) {
                                    gameService.makeShot(game_id, secondClient.namePlayer, firstClient.namePlayer, result);
                                }
                                break;
                            case "ENDGAME":
                                //команда вида: Client#N ENDGAME WIN_HP_COUNT WIN_SHOT_COUNT LOOSER_SHOT_COUNT
                                if (command[0].equals("Client#1")) {
                                    gameService.endGame(game_id, firstClient.namePlayer, Integer.parseInt(command[2]),
                                            Integer.parseInt(command[3]), secondClient.namePlayer, Integer.parseInt(command[4]));
                                } else {
                                    gameService.endGame(game_id, secondClient.namePlayer, Integer.parseInt(command[2]),
                                            Integer.parseInt(command[3]), firstClient.namePlayer, Integer.parseInt(command[4]));
                                }
                                System.out.println(command[0] + " won!" + " He saved " + Integer.parseInt(command[2]) + " HP" );
                                firstClient.outgoing.println(FINAL + command[0]);
                                secondClient.outgoing.println(FINAL+ command[0]);
                                firstClient.interrupt();
                                secondClient.interrupt();

                                break;
                            default:
                                System.out.println("Unexpected command, the incident will be recorded");
                                break;
                        }
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
