package service;

public interface GameService {
    Integer startGame(String firstPlayerName, String firstIp, String secondPlayerName, String secondIp);

    void makeShot(Integer gameId, String attackPlayerName, String defendingPlayerName, boolean hit);

    void endGame(Integer gameId, String winnerName, Integer winnerHPRemain, Integer
            winnerShotCount, String looserName, Integer loserShotCount);
}
