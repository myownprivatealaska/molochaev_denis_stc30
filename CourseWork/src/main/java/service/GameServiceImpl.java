package service;

import model.Game;
import model.Player;
import model.Shot;
import repository.*;

import javax.sql.DataSource;
import java.time.LocalDateTime;


public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;
    private final GamesRepository gamesRepository;
    private final ShotsRepository shotsRepository;

    private Player firstPlayer;
    private Player secondPlayer;
    private Game game;

    public GameServiceImpl(DataSource dataSource) {
        this.playersRepository = new PlayersRepositoryImpl(dataSource);
        this.gamesRepository = new GamesRepositoryImpl(dataSource);
        this.shotsRepository = new ShotsRepositoryImpl(dataSource);
    }

    @Override
    public Integer startGame(String firstPlayerName, String firstPlayerIp,
                             String secondPlayerName, String secondPlayerIp) {
        firstPlayer = getAndSavePlayerIfExist(firstPlayerName, firstPlayerIp);
        secondPlayer = getAndSavePlayerIfExist(secondPlayerName, secondPlayerIp);
        LocalDateTime sysdate = LocalDateTime.now();
        game = new Game(sysdate, firstPlayer, secondPlayer);
        System.out.println("New game was started at " + sysdate);
        System.out.println("First player is " + firstPlayer.getName()
                + ", second player is " + secondPlayer.getName() + "!");
        return gamesRepository.save(game).getId();
    }

    @Override
    public void makeShot(Integer gameId, String attackPlayerName, String defendingPlayerName, boolean hit) {
        if (!game.getId().equals(gameId)) {
            System.out.println("Unexpected case, check it bro!");
        }
        LocalDateTime sysdate = LocalDateTime.now().minusSeconds(10);
        Player attack = firstPlayer;
        Player defending = secondPlayer;
        if (firstPlayer.getName().equals(defendingPlayerName)) {
            attack = secondPlayer;
            defending = firstPlayer;
        }
        shotsRepository.save(new Shot(sysdate, hit, game, attack, defending));
    }

    @Override
    public void endGame(Integer gameId, String winnerName, Integer winnerHPRemain, Integer
            winnerShotCount, String looserName, Integer loserShotCount) {
        if (!game.getId().equals(gameId)) {
            System.out.println("Unexpected case, check it bro!");
        }
        //Для начала найдем поймем кто победитель, а кто побежденный
        Player winnerPlayer = firstPlayer;
        Player looserPlayer = secondPlayer;

        Integer firstCount = winnerShotCount;
        Integer secondCount = loserShotCount;
        if (firstPlayer.getName().equals(looserName)) {
            winnerPlayer = secondPlayer;
            looserPlayer = firstPlayer;
            firstCount = loserShotCount;
            secondCount = winnerShotCount;
        }

        game.setAp_shoots_count(firstCount);
        game.setBp_shoots_count(secondCount);
        //Обновляем статы игороков
        winnerPlayer.addStat(true, winnerHPRemain);
        looserPlayer.addStat(false, 0);

        gamesRepository.update(game);
        playersRepository.update(winnerPlayer);
        playersRepository.update(looserPlayer);
        System.out.println("SERVICE: Game over, results have been saved!");

    }


    private Player getAndSavePlayerIfExist(String playerName, String playerIp) {
        Player candidate = playersRepository.findByName(playerName);
        if (candidate != null) candidate.setIp(playerIp);
        else candidate = new Player(playerName, playerIp);
        return playersRepository.save(candidate);
    }
}
