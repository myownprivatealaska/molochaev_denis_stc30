import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import server.GameServer;
import service.GameService;
import service.GameServiceImpl;

public class ApplicationLauncher {
    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("postgres");
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(10);
        HikariDataSource dataSource = new HikariDataSource(config);
        GameService gameService = new GameServiceImpl(dataSource);

        GameServer gameServer = new GameServer(gameService);
        gameServer.start(8080);

    }
}
