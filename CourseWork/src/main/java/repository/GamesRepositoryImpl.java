package repository;

import model.Game;

import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryImpl implements GamesRepository {

    private final DataSource dataSource;

    private static final String ADD_GAME = "INSERT " +
            "INTO game(effective_date, a_player_id, b_player_id, a_player_shoots_count, b_player_shoots_count, duration)" +
            "VALUES (?,?,?,?,?,?);";
    private static final String SET_GAME_BY_ID = "UPDATE game  " +
            "SET a_player_shoots_count = ?, b_player_shoots_count = ?, " +
            "duration = current_timestamp - effective_date " +
            "where id = ?";

    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void update(Game game) {
        try (PreparedStatement statement = getStatementByQuery(SET_GAME_BY_ID, false)) {
            statement.setInt(1, game.getAp_shoots_count());
            statement.setInt(2, game.getBp_shoots_count());
            //Тут пришлось заколхозить вычисление времени current_timestamp  - e.effective_date
            //Путь самурая труден, ибо не написано удобных фрэймворков для таких вещей
            statement.setInt(3, game.getId());
            if (statement.executeUpdate() == 0)
                throw new SQLException("Game with id = " + game.getId() + " not found!");
            System.out.println("DEBUG: Game with id = " + game.getId() + " successfully updated!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Game save(Game game) {
        try (PreparedStatement statement = getStatementByQuery(ADD_GAME, true)) {
            statement.setTimestamp(1, Timestamp.valueOf(game.getEffective_date()));
            statement.setInt(2, game.getA_player().getId());
            statement.setInt(3, game.getB_player().getId());
            statement.setInt(4, game.getAp_shoots_count() == null ? 0 : game.getAp_shoots_count());
            statement.setInt(5, game.getBp_shoots_count() == null ? 0 : game.getBp_shoots_count());
            statement.setTime(6, game.getDuration() == null ? java.sql.Time.valueOf("00:00:00") : game.getDuration());
            if (statement.executeUpdate() == 0)
                throw new SQLException("Save was broken with some error!");
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                game.setId(resultSet.getInt("id"));
            } else throw new SQLException("Id of player is corrupt!");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return game;
    }

    private PreparedStatement getStatementByQuery(String query, boolean isSave) {
        try {
            Connection connection = dataSource.getConnection();
            if (isSave) return connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            return connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
