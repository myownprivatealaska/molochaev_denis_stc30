package repository;

import model.Shot;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ShotsRepositoryImpl implements ShotsRepository {

    private DataSource dataSource;

    private static final String ADD_SHOT = "INSERT INTO shot(date, hit, game_id, attack, defending)" +
            "VALUES (?,?,?,?,?);";

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(ADD_SHOT)) {
            statement.setTimestamp(1, java.sql.Timestamp.valueOf(shot.getDate()));
            statement.setBoolean(2, shot.isHit());
            statement.setInt(3, shot.getGame().getId());
            statement.setInt(4, shot.getAttack().getId());
            statement.setInt(5, shot.getDefending().getId());

            if (statement.executeUpdate() == 0)
                throw new SQLException("Shot is unsaved!");
            System.out.println("DEBUG: Shot from " + shot.getAttack().getName() + " to "
                    + shot.getDefending().getName() + " successfully saved!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
