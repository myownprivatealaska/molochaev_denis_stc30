package repository;

import model.Game;

public interface GamesRepository extends Repository<Game> {
    void update(Game game);

    @Override
    Game save(Game game);
}
