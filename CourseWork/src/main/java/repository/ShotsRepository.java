package repository;

import model.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
