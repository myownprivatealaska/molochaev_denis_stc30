package repository;

public interface Repository<T> {
    T save(T entity);
}
