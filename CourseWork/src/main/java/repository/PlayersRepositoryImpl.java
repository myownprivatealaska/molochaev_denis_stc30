package repository;

import model.Player;

import javax.sql.DataSource;
import java.sql.*;


public class PlayersRepositoryImpl implements PlayersRepository {

    private final DataSource dataSource;

    private static final String GET_PLAYER_BY_NAME = "SELECT * FROM player p WHERE p.name = ?;";
    private static final String ADD_PLAYER = "INSERT INTO player(ip, name) " +
            "VALUES (?,?);";
    private static final String SET_PLAYER_BY_ID = "UPDATE player " +
            "SET ip = ?, max_score = ?, win_count = ?, loose_count = ? " +
            "WHERE id = ?";

    RowMapper<Player> playerRowMapper = row -> {
        Player player = new Player();
        player.setId(row.getInt("id"));
        player.setIp(row.getString("ip"));
        player.setName(row.getString("name"));
        player.setMax_score(row.getInt("max_score"));
        player.setWin_count(row.getInt("win_count"));
        player.setLoose_count(row.getInt("loose_count"));
        return player;
    };

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void update(Player player) {
        try (PreparedStatement statement = getStatementByQuery(SET_PLAYER_BY_ID, false)) {
            statement.setString(1, player.getIp());
            statement.setInt(2, player.getMax_score() == null ? 0 : player.getMax_score());
            statement.setInt(3, player.getWin_count() == null ? 0 : player.getWin_count());
            statement.setInt(4, player.getLoose_count() == null ? 0 : player.getLoose_count());
            statement.setInt(5, player.getId());
            if (statement.executeUpdate() == 0)
                throw new SQLException("Player with name = " + player.getName() + " not found!");
            System.out.println("DEBUG: Player with name = " + player.getName() + " successfully updated!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Player findByName(String name) {
        try (PreparedStatement statement = getStatementByQuery(GET_PLAYER_BY_NAME, false)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return playerRowMapper.mapByResult(resultSet);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Player save(Player player) {
        /**
         * Проверяем на всякий случай, вдруг такой пользователь есть,
         * тогда нам нужно сделать update вместо save
         */
        if (findByName(player.getName()) != null) {
            System.out.println("DEBUG: При попытке сохранения пользователя была обнаружена его неуникальность!");
            update(player);
            return player;
        }
        try (PreparedStatement statement = getStatementByQuery(ADD_PLAYER, true)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());

            if (statement.executeUpdate() == 0)
                throw new SQLException("Сохранение было прервано из-за ошибки!");
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                player.setId(resultSet.getInt("id"));
            } else throw new SQLException("Id пользователя не было получено!");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }
    /**
     * Метод-обертка для уменьшения дублирования кода
     *
     * @param query  Стринг представление SQL запроса(UPDATE, SELECT, INSERT)
     * @param isSave Флаг, идентифицирует, нужно ли возрвщать сгенерированный БД ПК
     * @return
     */
    public PreparedStatement getStatementByQuery(String query, boolean isSave) {
        try {
            Connection connection = dataSource.getConnection();
            if (isSave) return connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            return connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
