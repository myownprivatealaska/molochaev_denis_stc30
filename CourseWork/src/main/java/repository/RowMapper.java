package repository;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
    T mapByResult(ResultSet row) throws SQLException;
}
