-- делаем таблицу пользователей:
create table player
(
    id          serial primary key,
    ip          varchar(15) not null DEFAULT '0.0.0.0',
    name        varchar(20), constraint unique_name unique (name),
    max_score   integer not null DEFAULT 0,
    win_count   integer not null DEFAULT 0,
    loose_count integer not null DEFAULT 0
);

-- делаем таблицу игр:
create table game
(
    id serial primary key,
    effective_date date,
    a_player_id integer, foreign key (a_player_id) references player(id),
    b_player_id integer, foreign key (b_player_id) references player(id),
    a_player_shoots_count integer not null DEFAULT 0,
    b_player_shoots_count integer not null DEFAULT 0,
    duration time
);

-- делаем таблицу тарссировки выстрелов
create table shot
(
    id serial primary key,
    date date,
    hit boolean default false,
    game_id integer, foreign key (game_id) references game(id),
    attack integer, foreign key (attack) references player(id),
    defending integer, foreign key (defending) references player(id)
);