import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] inputStrings = {"stop", "Co1MMon", "nigh2t visi1on"};
        int[] inputInts = {123, 2000, 258, 990, 302, 50021};
        NumbersAndStringProcessor inputData = new NumbersAndStringProcessor(inputInts, inputStrings);
        //1.1 Разворачиваем инты
        NumbersProcess firstNumberImplement = number -> {
            int result = 0;
            while (number > 0) {
                result = (result * 10) + (number % 10);
                number /= 10;
            }
            return result;
        };
        // 1.2 Убираем нули из интов
        NumbersProcess secondNumberImplement = number -> {
            int result = 0;
            int pow = 1;
            while (number > 0) {
                int mod = number % 10;
                if (mod != 0) {
                    result = result + (mod * pow);
                    pow *= 10;
                }
                number /= 10;
            }
            return result;
        };
        //1.3 Заменить четные цифры ближайшей нечетной снизу
        NumbersProcess thirdNumberImplement = number -> {
            int result = 0;
            int pow = 1;
            while (number > 0) {
                int mod = number % 10;
                if (mod%2 != 0 || mod == 0) {
                    result = result + (mod * pow);
                    pow *= 10;
                } else {
                    result = result + ((mod - 1) * pow);
                    pow *= 10;
                }
                number /= 10;
            }
            return result;
        };
        //2.1 Развернуть исходную строку
        StringsProcess firstStringImplement = input -> new StringBuilder(input).reverse().toString();
        //2.2 Убрать все цифры из строки
        StringsProcess secondStringImplement = input -> {
            StringBuilder temporaryString = new StringBuilder();
            int index = 0;
            while (index < input.length()) {
                char tempChar = input.charAt(index);
                if (!Character.isDigit(tempChar)) temporaryString.append(tempChar);
                index++;
            }
            return temporaryString.toString();
        };
        //2.3 Все маленькие буквы - большие
        StringsProcess thirdStringImplement = input -> {
            StringBuilder temporaryString = new StringBuilder();
            int index = 0;
            while (index < input.length()) {
                char tempChar = input.charAt(index);
                if (Character.isLowerCase(tempChar)) tempChar = Character.toUpperCase(tempChar);
                temporaryString.append(tempChar);
                index++;
            }
            return temporaryString.toString();
        };
        //Получаем результаты для интерфейса NumbersProcess
        int[] firstNumberResult = inputData.process(firstNumberImplement);
        int[] secondNumberResult = inputData.process(secondNumberImplement);
        int[] thirdNumberResult = inputData.process(thirdNumberImplement);

        //Получае результаты для интерфейса StringsProcess
        String[] firstStringResult = inputData.process(firstStringImplement);
        String[] secondStringResult = inputData.process(secondStringImplement);
        String[] thirdStringResult = inputData.process(thirdStringImplement);

        //Демонстрируем результаты по числовому интерфейсу
        System.out.println(Arrays.toString(firstNumberResult));
        System.out.println(Arrays.toString(secondNumberResult));
        System.out.println(Arrays.toString(thirdNumberResult));
        //Тоже самое, но по стрингам
        System.out.println(Arrays.toString(firstStringResult));
        System.out.println(Arrays.toString(secondStringResult));
        System.out.println(Arrays.toString(thirdStringResult));
     }
}
