import java.util.Arrays;

public class NumbersAndStringProcessor {
    private int[] intArray;
    private String[] stringsArray;


    public NumbersAndStringProcessor(int[] intArray, String[] stringsArray) {
        this.intArray = Arrays.copyOf(intArray, intArray.length);
        this.stringsArray = Arrays.copyOf(stringsArray, stringsArray.length);
    }

    public String[] process(StringsProcess stringsProcess) {
        String[] resultString = new String[this.stringsArray.length];
        for (int i=0; i<this.stringsArray.length; i++) {
            resultString[i] = stringsProcess.process(this.stringsArray[i]);
        }
        return resultString;
    }

    public int[] process(NumbersProcess numbersProcess) {
        int[] resultInt = new int[this.intArray.length];
        for (int i=0; i<this.intArray.length; i++) {
            resultInt[i] = numbersProcess.process(this.intArray[i]);
        }
        return resultInt;
    }

    public void showIntArray() {
        System.out.println(Arrays.toString(intArray));
    }

    public void showStringsArray() {
        System.out.println(Arrays.toString(stringsArray));
    }

}
