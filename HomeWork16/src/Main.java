import Graph.*;
import Tree.BinarySearchTreeImplement;

public class Main {
    public static void main(String[] args) {
        /**
         * BINARY SEARCH TREE DEMO
         */
        BinarySearchTreeImplement<Integer> tree = new BinarySearchTreeImplement<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);

//      tree.printDfs();
//      System.out.println("Демо удаления элемента");
//      tree.remove(6);
//      System.out.println("Результат после удаления");
//      tree.printDfs();
//      tree.printBfsLevel();

        /**
         * GRAPH DEMO
         */
        Vertex a0 = new VertexIntegerImp(0);
        Vertex a1 = new VertexIntegerImp(1);
        Vertex a2 = new VertexIntegerImp(2);
        Vertex a3 = new VertexIntegerImp(3);
        Vertex a4 = new VertexIntegerImp(4);

        Edge edge0 = new EdgeTwoVerticesImp(a0,a1, 3);
        Edge edge1 = new EdgeTwoVerticesImp(a0,a4, 8);
        Edge edge2 = new EdgeTwoVerticesImp(a0,a3, 7);
        Edge edge3 = new EdgeTwoVerticesImp(a4,a3, 3);
        Edge edge4 = new EdgeTwoVerticesImp(a1,a3, 4);
        Edge edge5 = new EdgeTwoVerticesImp(a1,a2, 1);
        Edge edge6 = new EdgeTwoVerticesImp(a3,a2, 2);

        GraphAdjacencyMatrixImpl orientedGraph = new GraphAdjacencyMatrixImpl();
        orientedGraph.addEdge(edge0);
        orientedGraph.addEdge(edge1);
        orientedGraph.addEdge(edge2);
        orientedGraph.addEdge(edge3);
        orientedGraph.addEdge(edge4);
        orientedGraph.addEdge(edge5);
        orientedGraph.addEdge(edge6);

        //orientedGraph.print();
        //System.out.println("Проверка, являются ли соседями вершины:");
        //System.out.println(orientedGraph.isAdjacent(a0,a3)); //TRUE
        //System.out.println(orientedGraph.isAdjacent(a0,a2)); //FALSE
        System.out.println("Демонстрация поиска 'в глубину':");
        orientedGraph.dfs(a0);
        System.out.println("Демонстрация поиска 'в ширину':");
        orientedGraph.bfs(a0);
        System.out.println("Демонстрация поиска кратчайшего пути по жалкому подобию алгоритма А*(A-STAR), " +
                "в перемешку с неграмотностью:");
        orientedGraph.likeA(a0, a3);
    }
}
