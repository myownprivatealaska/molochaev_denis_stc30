package Tree;

import java.util.Deque;
import java.util.LinkedList;

public class BinarySearchTreeImplement<T extends Comparable<T>> implements BinarySearchTree<T> {
    static class Node<E> {
        E value;
        Node<E> right, left;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    public Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        } else if (value.compareTo(root.value) < 0) {
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }


    @Override
    public void printDfs() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) stack.push(current.left);
            if (current.right != null) stack.push(current.right);
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);
        Node<T> current;
        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.println(current.value + " ");
            if (current.left != null) queue.add(current.left);
            if (current.right != null) queue.add(current.right);
        }
    }

    /**
     * Пришлось немного попотеть =)
     * Принцип прост, две очереди, одна хранит текущие узлы, другая потомков, в момент печати
     * потомков наполняется очередь узлов
     */
    public void printBfsLevel() {
        Deque<Node<T>> innerQ = new LinkedList<>();
        Deque<Node<T>> outerQ = new LinkedList<>();
        outerQ.add(root);
        Node<T> current;
        while (!outerQ.isEmpty()) {
            while (!outerQ.isEmpty()) innerQ.add(outerQ.pop());
            while (!innerQ.isEmpty()) {
                current = innerQ.pop();
                if (current.value == root.value) System.out.println(root.value);
                if (current.left != null) System.out.print(current.left.value + " ");
                else System.out.print("* ");
                if (current.right != null) System.out.print(current.right.value + " ");
                else System.out.print("* ");
                if (current.left != null) outerQ.add(current.left);
                if (current.right != null) outerQ.add(current.right);
            }
            System.out.println();
        }
    }


    public void dfs(Node<T> root) {
        if (root != null) {
            dfs(root.left);
            System.out.print(root.value + " ");
            dfs(root.right);
        }
    }

    /**
     * Решается путем использования метода Т. Хиббарда: удаляемый узел,
     * если он имеет поддеревья заменяется на наименьший узел в
     * правом поддереве
     *
     * @param value
     */
    @Override
    public void remove(T value) {
        root = remove(root, value);
    }

    public Node<T> remove(Node<T> root, T value) {
        if (root == null) return null;
        if (value.compareTo(root.value) < 0) {
            root.left = remove(root.left, value);
        } else if (value.compareTo(root.value) > 0) {
            root.right = remove(root.right, value);
        } else {
            if (root.left == null) return root.right;
            if (root.right == null) return root.left;
            Node<T> temporary = root;
            root = findMinimal(temporary.right);
            root.right = removeMinimal(temporary.right);
            root.left = temporary.left;
        }
        return root;
    }

    public Node<T> findMinimal(Node<T> node) {
        if (node.left == null) return node;
        return findMinimal(node.left);
    }

    public Node<T> removeMinimal(Node<T> node) {
        if (node.left == null) return node.right;
        node.left = removeMinimal(node.left);
        return node;
    }

    @Override
    public boolean contain(T value) {
        return contain(root, value);
    }

    public boolean contain(Node<T> root, T value) {
        if (root == null) return false;
        if (value.compareTo(root.value) < 0) {
            return contain(root.left, value);
        } else if (value.compareTo(root.value) > 0) {
            return contain(root.right, value);
        } else if (value.compareTo(root.value) == 0) return true;
        return false;
    }
}
