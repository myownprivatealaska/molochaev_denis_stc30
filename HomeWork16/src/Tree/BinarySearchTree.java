package Tree;

public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    void printDfs();
    void printDfsByStack();

    /**
     * Самостоятельная реализация
     */
    void printBfs();
    void remove(T value);
    boolean contain(T value);
}
