package Graph;

public interface OrientedGraph {
    void addVertex(Vertex vertex);
    void addEdge(Edge edge);
    boolean isAdjacent(Vertex a, Vertex b);
    void print();
}
