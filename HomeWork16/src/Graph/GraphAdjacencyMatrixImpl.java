package Graph;

import java.util.*;

public class GraphAdjacencyMatrixImpl implements OrientedGraph {
    private static final int MAX_VERTICES_COUNT = 5;
    private int count;
    private int[][] matrix = new int[MAX_VERTICES_COUNT][MAX_VERTICES_COUNT];
    private HashMap<Integer, LinkedList<Neighbors>> adjacentList;


    public GraphAdjacencyMatrixImpl() {
        this.adjacentList = new HashMap<Integer, LinkedList<Neighbors>>();
    }

    @Override
    public void addVertex(Vertex vertex) {
        this.count++;
    }

    @Override
    public void addEdge(Edge edge) {
        putAdjacentList(edge);
        matrix[edge.getFirst().getNumber()][edge.getSecond().getNumber()] = edge.weight();
    }

    @Override
    public boolean isAdjacent(Vertex a, Vertex b) {
        if (adjacentList.get(a.getNumber()).stream().anyMatch(entity -> entity.vertexId == b.getNumber())) return true;
        return false;
    }

    public boolean isAdjacent(int a, int b) {
        if (adjacentList.get(a).stream().anyMatch(entity -> entity.vertexId == b)) return true;
        return false;
    }

    /**
     * ЗАДАНИЕ ADJACENT LIST
     *
     * @param edge
     */
    private void putAdjacentList(Edge edge) {
        Integer indexV = edge.getFirst().getNumber();
        Neighbors neighbors = new Neighbors(edge);
        if (!adjacentList.containsKey(indexV)) {
            adjacentList.put(indexV, new LinkedList<>());
        }
        adjacentList.get(indexV).add(neighbors);
    }

    @Override
    public void print() {
        System.out.print("  ");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private class Neighbors {
        private int vertexId;
        private int weight;

        public Neighbors(Edge edge) {
            this.vertexId = edge.getSecond().getNumber();
            this.weight = edge.weight();
        }
    }

    /**
     * ЗАДАНИЕ DFS
     * Фасадный метод, который вызывает реальный метод и передает массив типа boolean
     * в котором вершина это индекс, а значение это true в случае посещения данного узла,
     * в противном случае false, длинна масива это размер матрицы смежности
     *
     * @param vertex
     */
    public void dfs(Vertex vertex) {
        boolean[] visited = new boolean[MAX_VERTICES_COUNT];
        System.out.println("Из вершины " + vertex.getNumber() + " доступны: ");
        dfsEngine(vertex.getNumber(), visited);
    }

    /**
     * Рекурсивно ищем все вершины, которые мы можем достичь в направленном графе
     * отличие от БФС в том, что мы стремимся в глубину
     *
     * @param input   файер-стартер
     * @param visited
     */
    public void dfsEngine(int input, boolean[] visited) {
        Deque<Integer> stack = new LinkedList<>();
        stack.push(input);
        while (!stack.isEmpty()) {
            int current = stack.poll();
            if (!visited[current]) {
                System.out.println("вершина #" + current);
                visited[current] = true;
            }
            if (adjacentList.containsKey(current))
                adjacentList.get(current).forEach(neighbors ->
                        stack.push(neighbors.vertexId));
        }
    }

    /**
     * ЗАДАНИЕ BFS
     * Работает так же как и BFS, но принцип похож на капилярный эффект:
     * мочим в вине край салфетки и вся салфетка постепенно окрашевается из этого края
     */
    public void bfs(Vertex vertex) {
        boolean[] visited = new boolean[MAX_VERTICES_COUNT];
        System.out.println("Из вершины " + vertex.getNumber() + " доступны: ");
        bfsEngine(vertex.getNumber(), visited);
    }

    public void bfsEngine(int input, boolean[] visited) {
        Deque<Integer> queue = new LinkedList<>();
        queue.add(input);
        while (!queue.isEmpty()) {
            int current = queue.pop();
            if (!visited[current]) {
                System.out.println("вершина #" + current);
                visited[current] = true;
            }
            if (adjacentList.containsKey(current))
                adjacentList.get(current).forEach(neighbors ->
                        queue.add(neighbors.vertexId));
        }
    }

    /**
     * Метод-урод иммитирует обход графа алгоритмом А* без эвристической функции,
     * что-то вроде алоритма Дейкстры с очередью. Принцип:
     * - создаем пустую таблицу маршрута вида массив[вторая вершина] = вес ребра + массив[первая вершина]
     * - помещаем первый элемент в очередь
     * - пока очередь не пуста, извлекаем очередной элемент
     * - проверяем что это не результат и что этот элемент есть в adjacency list
     * - форичем проходимся по всем потомкам, заполняя массив маршрута(см алгоритм выше) и
     * заполняем хэшмап текуших значений, что бы не исползовть очередь с приоритетами используем хэшмап, где:
     * ключь это вес, а значение это индекс, найдя меньший ключ, находим самую "дешевую вершину"
     * - берем самую дешевую вершину и помещаем в очередь
     * - пишем трассировку
     * - если мы дошли до искомого элемента - пишем вес перемещения
     * - если мы не нашли adjacency list текущее значение индекса вершины - значит путь недостежим
     *
     * @param in из какой вершины ищем кротчайший путь
     * @param to в какую вершину ищем кротчайший путь
     *           Этот метод урод потому, что он не покрывает частные случаи
     */
    public void likeA(Vertex in, Vertex to) {
        Deque<Integer> queue = new LinkedList<>();
        int[] path = new int[MAX_VERTICES_COUNT];
        if (!adjacentList.containsKey(in.getNumber()) &&
                !adjacentList.containsKey(to.getNumber()))
            System.out.println("Стартовая или конечная вершина не существует!");
        queue.add(in.getNumber());
        System.out.println("Поиск кратчайшего пути из вершины " + in.getNumber() + " в вершину " + to.getNumber());
        System.out.print(in.getNumber());
        while (!queue.isEmpty()) {
            int currentVertexId = queue.poll();
            if (to.getNumber() == currentVertexId) {
                System.out.println("\nВес минимального пути: " + path[currentVertexId]);
                break;
            }
            if (!adjacentList.containsKey(currentVertexId)) {
                System.out.println("\nПути не существует");
                break;
            }
            Map<Integer, Integer> findMin = new HashMap<>();
            for (Neighbors neighbors : adjacentList.get(currentVertexId)) {
                path[neighbors.vertexId] = neighbors.weight + path[currentVertexId];
                findMin.put(path[neighbors.vertexId], neighbors.vertexId);
            }
            int min = findMin.keySet().stream().min((s1, s2) -> s1 - s2).get();
            System.out.print(" --> " + findMin.get(min));
            queue.add(findMin.get(min));
        }
    }
}
