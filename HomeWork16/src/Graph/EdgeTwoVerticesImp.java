package Graph;

import java.util.Objects;

public class EdgeTwoVerticesImp implements Edge {
    private Vertex firstVertex;
    private Vertex secondVertex;
    private int weight;

    public EdgeTwoVerticesImp(Vertex firstVertex, Vertex secondVertex, int weight) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.weight = weight;
    }

    @Override
    public Vertex getFirst() {
        return firstVertex;
    }

    @Override
    public Vertex getSecond() {
        return secondVertex;
    }

    @Override
    public int weight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EdgeTwoVerticesImp that = (EdgeTwoVerticesImp) o;
        return weight == that.weight &&
                Objects.equals(firstVertex, that.firstVertex) &&
                Objects.equals(secondVertex, that.secondVertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstVertex, secondVertex, weight);
    }
}
