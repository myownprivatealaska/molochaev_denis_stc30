package Graph;

public class VertexIntegerImp implements Vertex {
    private int number;

    public VertexIntegerImp(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }
}
