package stc30.dmolochaev.javafx.client.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.MotionBlur;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import stc30.dmolochaev.javafx.client.socket.SocketClient;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ApplicationController implements Initializable {
    private Integer ownShotCount = 0;
    private Integer enemyShotCount = 0;

    private SocketClient socketClient;

    boolean isGameOver = false;

    @FXML
    private AnchorPane pane;

    @FXML
    private Button connectButton;

    @FXML
    private TextField playerNameTextField;

    @FXML
    private Button startGameButton;

    @FXML
    private TextArea logServerTextArea;

    @FXML
    private ImageView enemyAirCraft;

    @FXML
    private ImageView ownAirCraft;

    @FXML
    private ImageView water;

    @FXML
    private Label playerHP;

    @FXML
    private Label enemyHP;

    public ImageView getEnemyAirCraft() {
        return enemyAirCraft;
    }


    public EventHandler<KeyEvent> keyEventEventHandler = event -> {

        if (event.getCode() == KeyCode.LEFT) {
            ownMovement("LEFT");
        } else if (event.getCode() == KeyCode.RIGHT) {
            ownMovement("RIGHT");
        } else if (event.getCode() == KeyCode.CONTROL && !isGameOver) {
            ++ownShotCount;
            Circle bullet = new Circle();
            bullet.setRadius(5);
            bullet.setLayoutX(ownAirCraft.getX() + ownAirCraft.getLayoutX() + (ownAirCraft.getFitWidth() / 2));
            bullet.setLayoutY(ownAirCraft.getLayoutY() - 5);
            bullet.setFill(Color.RED);
            pane.getChildren().add(bullet);

            MotionBlur motionBlur = new MotionBlur();
            motionBlur.setRadius(13.0);
            motionBlur.setAngle(0);

            Timeline timeline = new Timeline();
            KeyFrame keyFrame = new KeyFrame(Duration.seconds(0), animation -> {
                bullet.setLayoutY(bullet.getLayoutY() - 1);
                bullet.setEffect(motionBlur);

                if (bullet.getBoundsInParent().intersects(enemyAirCraft.getBoundsInParent())) {
                    bullet.setVisible(false);
                    pane.getChildren().remove(bullet);


                    //Client#N ENDGAME WIN_HP_COUNT WIN_SHOT_COUNT LOOSER_SHOT_COUNT
                    Platform.runLater(() -> {
                                int countHP = Integer.parseInt(getEnemyHP().getText());
                                if (--countHP == 0)
                                    socketClient.sendCommandToServer("ENDGAME " + playerHP.getText() + " "
                                            + ownShotCount + " " + enemyShotCount);
                                getEnemyHP().setText(String.valueOf(countHP));
                            }
                    );

                    ownShotResult(true);
                    timeline.stop();
                } else if (bullet.getLayoutY() <= enemyAirCraft.getLayoutY()) {
                    bullet.setVisible(false);
                    pane.getChildren().remove(bullet);

                    ownShotResult(false);
                    timeline.stop();
                }
            });

            socketClient.sendCommandToServer("SHOT_T");

            timeline.getKeyFrames().addAll(keyFrame, new KeyFrame(Duration.seconds(0.005)));
            timeline.setCycleCount(340);
            timeline.play();
        }

    };

    private void ownShotResult(boolean isHit) {
        String result = isHit ? "HIT" : "MISSED";
        socketClient.sendCommandToServer("SHOT_R " + result);
    }

    private void ownMovement(String side) {
        if (!isGameOver) {
            int step = 6;
            if (side.equals("LEFT")) step *= -1;
            socketClient.sendCommandToServer("MOVE " + side);
            ownAirCraft.setX(ownAirCraft.getX() + step);
        }
    }


    public TextArea getLogServerTextArea() {
        return logServerTextArea;
    }

    public void insertToLogTextArea(String command) {
        getLogServerTextArea().insertText(0, LocalDateTime.now() + ": " + command + "\n");
    }

    public TextField getPlayerNameTextField() {
        return playerNameTextField;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logServerTextArea.setEditable(false);
        startGameButton.setDisable(true);
        playerNameTextField.setDisable(true);
        /**
         * Поднимаем сокет и дисэйблим кнопку, просто косметика
         */
        connectButton.setOnAction(event -> {
            connectButton.setDisable(true);
            startGameButton.setDisable(false);
            insertToLogTextArea("Try to get connection");
            socketClient = new SocketClient("127.0.0.1", 8080, this);
            playerNameTextField.setDisable(false);
        });

        /**
         * Фактически мы проверяем поле на пустое имя и отправляем его командой на сервер
         */
        startGameButton.setOnAction(event -> {
            if (getPlayerNameTextField().getText().length() == 0)
                insertToLogTextArea("Name is not fill");
            else {
                String playerName = getPlayerNameTextField().getText();
                socketClient.sendCommandToServer("START " + playerName);
                insertToLogTextArea("Your nickname was set as " + playerName);
                startGameButton.setDisable(true);
                playerNameTextField.setDisable(true);
                startGameButton.getScene().getRoot().requestFocus();
            }
        });
    }

    public AnchorPane getPane() {
        return pane;
    }

    public ImageView getOwnAirCraft() {
        return ownAirCraft;
    }

    public Label getPlayerHP() {
        return playerHP;
    }

    public void setPlayerHP(Label playerHP) {
        this.playerHP = playerHP;
    }

    public Label getEnemyHP() {
        return enemyHP;
    }

    public void setEnemyHP(Label enemyHP) {
        this.enemyHP = enemyHP;
    }

    public Integer getEnemyShotCount() {
        return enemyShotCount;
    }

    public void setEnemyShotCount(Integer enemyShotCount) {
        this.enemyShotCount = enemyShotCount;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }
}
