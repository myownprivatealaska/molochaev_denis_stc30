package stc30.dmolochaev.javafx.client.socket;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.effect.MotionBlur;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import stc30.dmolochaev.javafx.client.controller.ApplicationController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {
    public ApplicationController controller;

    private Socket client;

    private PrintWriter outgoing;
    private BufferedReader incoming;

    public SocketClient(String host, int port, ApplicationController controller) {
        try {
            this.controller = controller;
            client = new Socket(host, port);
            outgoing = new PrintWriter(client.getOutputStream(), true);
            incoming = new BufferedReader(new InputStreamReader(client.getInputStream()));

            new Thread(asyncMessageTaskProcessor).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Функция для отправки команд на сервер
     *
     * @param command
     */
    public void sendCommandToServer(String command) {
        if (command != null && !client.isClosed())
            outgoing.println(command);
    }

    private Runnable asyncMessageTaskProcessor = () -> {
        while (true) {
            try {
                while (!client.isClosed()) {
                    String commandFromServer = incoming.readLine();
                    //Принимаем все как есть и вставляем методом-оберткой в логер-зону
                    if (commandFromServer != null) {
                        controller.insertToLogTextArea(commandFromServer);
                        //Тут уже начинаем разгоняться и добавляем движения по сторонам для опонента
                        String[] parser = commandFromServer.split(" ");
                        switch (parser[0]) {
                            case "MOVE":
                                enemyMovement(parser[1]);
                                break;
                            case "BANG":
                                controller.setEnemyShotCount(controller.getEnemyShotCount() + 1);

                                Platform.runLater(() -> {
                                    Circle bullet = new Circle();
                                    bullet.setRadius(5);
                                    bullet.setLayoutX(controller.getEnemyAirCraft().getX()
                                            + controller.getEnemyAirCraft().getLayoutX()
                                            + (controller.getEnemyAirCraft().getFitWidth() / 2));
                                    bullet.setLayoutY(controller.getEnemyAirCraft().getLayoutY() + 60);
                                    bullet.setFill(Color.YELLOW);
                                    controller.getPane().getChildren().add(bullet);

                                    MotionBlur motionBlur = new MotionBlur();
                                    motionBlur.setRadius(10.0);
                                    motionBlur.setAngle(10);

                                    Timeline timeline = new Timeline();
                                    KeyFrame keyFrame = new KeyFrame(Duration.seconds(0), animation -> {
                                        bullet.setLayoutY(bullet.getLayoutY() + 1);
                                        bullet.setEffect(motionBlur);

                                        if (bullet.getBoundsInParent().intersects(controller.getOwnAirCraft().getBoundsInParent())) {
                                            bullet.setVisible(false);

                                            controller.getPane().getChildren().remove(bullet);
                                            Platform.runLater(() -> controller.getPlayerHP()
                                                    .setText(String.valueOf(Integer.parseInt(controller.getPlayerHP().getText()) - 1))
                                            );

                                            timeline.stop();
                                        } else if (bullet.getLayoutY() > controller.getOwnAirCraft().getLayoutY() + 40) {
                                            bullet.setVisible(false);
                                            controller.getPane().getChildren().remove(bullet);
                                            timeline.stop();
                                        }
                                    });

                                    timeline.getKeyFrames().addAll(keyFrame, new KeyFrame(Duration.seconds(0.005)));
                                    timeline.setCycleCount(350);
                                    timeline.play();
                                });
                                break;
                            case "STOP":
                                //Вот тут надо как-то по-умному закрывать соединение
                                client.close();
                                controller.setGameOver(true);
                                break;
                        }

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    };


    private void enemyMovement(String side) {
        int step = 6;
        if (side.equals("LEFT")) step *= -1;
        controller.getEnemyAirCraft().setX(controller.getEnemyAirCraft().getX() + step);
    }

}
