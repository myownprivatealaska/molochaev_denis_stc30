public class Main {

    public static void main(String[] args) {
        Map<String, Integer> map = new MyHashMap<>();

        map.put("Марсель", 26);
        System.out.println(map.get("Марсель"));
        map.put("Марсель", 27);
        System.out.println(map.get("Марсель"));


        Set<String> set = new MyHashSet<>();
        set.add("Иван");
        set.add("Иванович");
        set.add("Иванов");
        set.add("Костя");

        System.out.println("Костя есть? - " + set.contains("Костя"));
        System.out.println("Андрей есть есть? - " + set.contains("Андрей"));
    }
}
