/**
 * Публичный интерфейс Map параметризованный двумя параметрами типами(ключ и значение) для реализации HashMap
 * @param <K>
 * @param <V>
 */
public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
}
