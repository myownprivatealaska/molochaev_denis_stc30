public class MyHashSet<V> implements Set<V> {
    /**
     * Для того, что бы работать на основе HashMap нам нужен
     * неизменяемый объект-заглушка для Value
     * и переменная, которая будет хранить HashMap
     */
    private final Object DUMMY = new Object();

    MyHashMap<V, Object> map;

    public MyHashSet() { map = new MyHashMap<>(); }

    @Override
    public void add(V value) {
        map.putHashSet(value, DUMMY);
    }

    @Override
    public boolean contains(V value) {
        if (map == null) return false;
        if (map.get(value) == DUMMY) return true;
        else return false;
    }
}
