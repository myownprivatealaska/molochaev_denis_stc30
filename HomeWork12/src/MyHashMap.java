import java.util.Arrays;

public class MyHashMap<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 1 << 4;
    private MapEntry<K, V> entry[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntryDummy = new MapEntry<>(key, value);
        int index = getIndexByKey(key);
        if (entry[index] == null) {
            entry[index] = newMapEntryDummy;
        } else {
            K currentKey = entry[index].getKey();
            if (currentKey.equals(key)) entry[index].setValue(value);
            else System.out.println("Хеш текущего ключа коррелирует с параметром key, " +
                    "но значения различаются, эту ситуацию мы не обрабатываем, но подсвечиваем");
        }
    }


    /**
     * Этот метод оставлю для работы хэшсета, по хорошему put должен быть круче,
     * иметь больше формальных аргументов и предусматривать
     * ресайз при корреляции ключей, флаги на перезапись в случае кореляции значений и тд.
     *
     * @param key
     * @param value
     */
    public void putHashSet(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = getIndexByKey(key);
        if (entry[index] == null) {
            entry[index] = newMapEntry;
        } else {
            K currentKey = entry[index].getKey();
            if (!currentKey.equals(key)) {
                MapEntry<K, V> tempEntry[] = entry;
                entry = new MapEntry[entry.length << 1];
                for (int i = 0; i < tempEntry.length; i++) {
                    MapEntry<K, V> current = tempEntry[i];
                    if (current != null) put(current.getKey(), current.getValue());
                }
            }
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entry.length - 1);
        /**
         * Ситуация аналогичная put, только наоборот, исключаем совпадения по хэшам и нулл
         */
        if (this.entry[index]!=null && this.entry[index].getKey().equals(key)) return this.entry[index].getValue();
        return null;
    }


    public int getIndexByKey(K key) {
        return key.hashCode() & (entry.length - 1);
    }

}


