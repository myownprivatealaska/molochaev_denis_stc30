/**
 * Публичный интерфейс Set параметризованный одним параметром типа(значение) для реализации HashSet
 * @param <V>
 */
public interface Set<V> {
    void add(V value);
    boolean contains(V value);
}
