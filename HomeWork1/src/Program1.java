public class Program1 {

    public static void main(String[] args) {
        int number = 54321;
        int sum = 0;
        System.out.print("Сумма цифр числа " + number);
        for (int i = 10000; i >= 1; i /= 10) {
            int temp = number / i;
            sum += temp;
            number -= temp * i;
        }
        System.out.println(" равна " + sum);
    }
}
