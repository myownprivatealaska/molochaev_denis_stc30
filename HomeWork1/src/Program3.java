import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int total = 1;
        System.out.println("Пожалуйста, введите последовательность чисел, для завершения введите 0 ...");
        int currentNumber = scanner.nextInt();
        while (currentNumber != 0) {
            int candidateNumber = currentNumber;
            int sumOfDigit = 0;
            for (int i = 100000; i >= 1; i /= 10) {
                int divCurrentNumber = candidateNumber / i;
                if (divCurrentNumber != 0) {
                    sumOfDigit += divCurrentNumber;
                    candidateNumber -= divCurrentNumber * i;
                }
            }
            boolean isPrime = true;
            for (int i = 2; i < sumOfDigit / 2; i++) {
                if (sumOfDigit % i == 0) isPrime = false;
                break;
            }
            if (isPrime) total *= currentNumber;
            currentNumber = scanner.nextInt();
        }
        System.out.println("Ответ: " + total);
    }
}
