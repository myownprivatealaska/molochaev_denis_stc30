class Program2_dummy {
    public static void main(String[] args) {
        int number = 12345;
        long binaryNumber = 0l;
        System.out.print("Число " + number + " в довичном представлении имеет вид: ");
        if (number >= 65536) {
            binaryNumber = 1_0000_0000_0000_0000l;
            number = number - 32768;
        }
        if (number >= 32768) {
            binaryNumber += 1_000_000_000_000_000l;
            number -= 32768;
        }
        if (number >= 16384) {
            binaryNumber += 100_0000_0000_0000l;
            number -= 16384;
        }
        if (number >= 8192) {
            binaryNumber += 10_0000_0000_0000l;
            number -= 8192;
        }
        if (number >= 4096) {
            binaryNumber += 1_0000_0000_0000l;
            number -= 4096;
        }
        if (number >= 2048) {
            binaryNumber += 1000_0000_0000l;
            number -= 2048;
        }
        if (number >= 1024) {
            binaryNumber += 100_0000_0000l;
            number -= 1024;
        }
        if (number >= 512) {
            binaryNumber += 10_0000_0000l;
            number -= 512;
        }
        if (number >= 256) {
            binaryNumber += 1_0000_0000l;
            number -= 1024;
        }
        if (number >= 128) {
            binaryNumber += 1000_0000l;
            number -= 128;
        }
        if (number >= 64) {
            binaryNumber += 100_0000l;
            number -= 64;
        }
        if (number >= 32) {
            binaryNumber += 10_0000l;
            number -= 32;
        }
        if (number >= 16) {
            binaryNumber += 1_0000l;
            number -= 16;
        }
        if (number >= 8) {
            binaryNumber += 1000l;
            number -= 8;
        }
        if (number >= 4) {
            binaryNumber += 100l;
            number -= 4;
        }
        if (number >= 2) {
            binaryNumber += 10l;
            number -= 2;
        }
        if (number == 1) {
            binaryNumber += 1l;
        }
        System.out.println(binaryNumber);
    }
}
