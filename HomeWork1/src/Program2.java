public class Program2 {
    public static void main(String[] args) {
        int number = 54321;
        long power = 10;
        int base = 2;
        long result = 0;
        System.out.print("Число " + number + " в двоичном представлении равно: ");
        if (number == 1) result = 1;
        while (number > 1) {
            if (number > base) {
                power = power * 10;
                base = base * 2;
            } else if (number <= base) {
                if (number < base) {
                    result = result + (power / 10);
                    number = number - (base / 2);
                    if (number == 1) result = result + 1;
                } else {
                    result = result + power;
                    number = number / base;
                }
                power = 10;
                base = 2;
            }
        }
        System.out.println(result);
    }
}
