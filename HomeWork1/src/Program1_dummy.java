public class Program1_dummy {
    public static void main(String[] args) {
        int number = 54321;
        int digitsSum = 0;
        int mod = 0;

        mod = number / 10000;
        digitsSum += mod;
        number -= mod * 10000;

        mod = number / 1000;
        digitsSum += mod;
        number -= mod * 1000;

        mod = number / 100;
        digitsSum += mod;
        number -= mod * 100;

        mod = number / 10;
        digitsSum += mod;
        number -= mod * 10;

        digitsSum += number;
        System.out.println(digitsSum);
    }
}
