import java.util.Objects;

/**
 * Модель для представления данных из файла в виде объекта
 */
public class Car {
    private int number;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public Car(int number, String model, String color, int mileage, int price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }
    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Номер " + number +
                ", модель '" + model + '\'' +
                ", цвет '" + color + '\'' +
                ", пробег " + mileage +
                ", цена " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return number == car.number &&
                mileage == car.mileage &&
                price == car.price &&
                Objects.equals(model, car.model) &&
                Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, mileage, price);
    }
}
