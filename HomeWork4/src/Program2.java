class Program2 {
    public static void main(String[] args) {
        int arrayLesson[] = {-32, -23, -6, 7, 22, 32, 123};
        int number = -32;
        int min = 0;
        int max = arrayLesson.length - 1;
        System.out.println("Число " + number + " находится на позиции " + binarySearcher(arrayLesson, min, max, number));
    }

    public static int binarySearcher(int[] array, int min, int max, int sought) {
        if (min <=  max) {
            int mid = min + (max - min) / 2;
            if (array[mid] < sought) {
                return binarySearcher(array, mid + 1, max, sought);
            } else if (array[mid] > sought) {
                return binarySearcher(array, min, mid - 1, sought);
            } return mid;
        }
        //Исключение на случай искомого, выходящего из диапазона чисел массива
        return -1;
    }
}