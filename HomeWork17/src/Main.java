import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    /**
     * Что б хоть что-то попало в visual vm лог, тк операция слишком быстро работает!
     * Пришлось добавить обработку исключений
     */
    public static double sumOfArray(Double[] array) throws InterruptedException {
        Double result = 0.0;
        for (double v : array) {
            result += v;
        }
        Thread.sleep(2000);
        return result;
    }

    public static void main(String[] args) {
        /**
         * Подготовка массива даблов и наполнение его рэндомными числами
         */
        Random random = new Random();
        int start = 0;
        int end = 5000000; //бьем по 100
        boolean isFin = false;
        Double[] targetArray = new Double[8000000];
        for (int i = 0; i < targetArray.length; i++)
            targetArray[i] = random.nextDouble();
        Scanner scanner = new Scanner(System.in);
        scanner.next();
        System.out.println("Сумма элементов массива: " + Arrays.stream(targetArray).reduce(Double::sum).get());
        ThreadService threadService = new ThreadService(0.0);
        while (!isFin) {
            int t_from = start;
            int t_end = end;
            if (end >= targetArray.length) {
                t_from = start;
                t_end = targetArray.length;
                isFin = true;
            }

            final int from = t_from;
            final int to = t_end;
            threadService.submit(() -> {
                final Double sum;
                try {
                    sum = sumOfArray(Arrays.copyOfRange(targetArray, from, to));
                    System.out.println(threadService.getName() + ": от: "
                            + from + " до: " + to + ", сумма равна: " + sum);
                    threadService.result += sum;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            start = end;
            end += 1000000;
            /**
             * isFin - флаг, что весь массив мы покрамсали, ждем пока все "левые треды" отработают вернем итог
             */
            if (isFin) {
                while (Thread.activeCount() > 2) {
                    continue;
                }
                System.out.println("Сумма по thread: " + threadService.result);
            }
        }
    }
}