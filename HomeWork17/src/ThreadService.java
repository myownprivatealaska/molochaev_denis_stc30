public class ThreadService {
    public Double result;

    public ThreadService(Double result) {
        this.result = result;
    }

    public String getName() {
       return Thread.currentThread().getName();
    }

    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
