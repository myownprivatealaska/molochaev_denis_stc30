public class LinkedList implements List {

    private Node first;
    private Node last;
    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator {
        Node current = first;
        @Override
        public int next() {
            if (current != null && this.hasNext()) {
                int returnValue = current.value;
                current = current.next;
                return returnValue;
            }
            return current.value;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }
    }

    @Override
    public int get(int index) {
        if (index > 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;
            while (i < index) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Этого элемента нет!");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current != null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index > 1 && index <= this.size()) {
            Node current = first;
            Node previous = first;
            int i = 1;
            while (index > i) {
                previous = current;
                current = current.next;
                i++;
            }
            previous.next = current.next;
            count--;
        } else if (index == 1) {
            first = first.next;
            count--;
        }
        else System.err.println("Такого индекса нет!");
    }

    @Override
    public void insert(int element, int index) {
        if (index > 0 && index <= this.size()) {
            Node current = first;
            int i = 1;
            while (index > 1) {
                current = current.next;
            }
            Node newNode = new Node(element);
            newNode.next = current.next;
            current.next = newNode;
            count++;
        }
        else System.out.println("Такого индекса нет!");
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        while (current != null) {
            if (current.value == element) return true;
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        Node current = first;
        Node previous = first;
        boolean found = false;
        for (int i=1; i < this.size(); i++) {
            if (current.value == element) {
                previous.next = current.next;
                found = true;
                break;
            }
            previous = current;
            current = current.next;
        }
        if (found) count--;
        else System.err.println("Такого элемента нет!");
    }

    public void revers() {
        if (size()==1) System.out.println("Готово!");
        Node latest = first;
        for (int i = 1; i < size(); i++) {
            Node challenger = first.next;
            first.next=last.next;
            last.next=first;
            first = challenger;
        }
        first = last;
        last = latest;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
