public class ArrayList implements List {
    private static final int MAX_ELEMENT = 10;
    private int data[];
    private int count;

    public ArrayList() {
        this.data = new int[MAX_ELEMENT];
    }

    private class ArrayListIterator implements Iterator {
        private int current = 0;
        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int get(int index) {
        return index < count ? data[index] : -1;
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) return i;
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index != -1) {
            for (int i = index; i < count - 1; i++) {
                data[i] = data[i + 1];
            }
            count--;
        } else {
            System.err.println("Элемент отсутствует в списке");
        }
    }

    @Override
    public void insert(int element, int index) {
        if (index > 0 && index < data.length) {
            if ((size()+1) == data.length) {
                resize();
            }
            int temp = data[index];
            for (int i = index; i <= count; i++){
                data[i] = element;
                element = data[i + 1];
                data[i + 1] = temp;
            }
            count++;
        }
        System.out.println("Индекс за пределы списка!");
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) resize();
        data[count] = element;
        count++;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    public void resize() {
        int oldCapacity = data.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);

        int[] newData = new int[newCapacity];
        System.arraycopy(this.data, 0, newData, 0, oldCapacity);
        this.data = newData;
    }


    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
