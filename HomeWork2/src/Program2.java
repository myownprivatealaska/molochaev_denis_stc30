import java.util.Scanner;
import java.util.Arrays;

class Program2 {
    public static void main(String[] args) {
        System.out.print("Размер ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int pooledValue = 0;
        for (int i=0; i<array.length; i++) {
            System.out.print("Элемент " + i + " - ");
            array[i] = scanner.nextInt();
        }
        System.out.println("Исходный массив имеет вид " + Arrays.toString(array));
        for (int i=0; i<array.length/2; i++) {
            pooledValue = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = pooledValue;
        }
        System.out.println("Развернутый массив " + Arrays.toString(array)); // выводится массив в зеркальном представлении }
    }
}
