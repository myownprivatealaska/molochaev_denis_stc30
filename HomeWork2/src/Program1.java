import java.util.Scanner;

class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Укажите размер массива ");
        int n = scanner.nextInt();
        int array[] = new int[n];
        int arraySum = 0;
        for (int i=0; i<array.length; i++) {
            System.out.print("Укажите элемент " + i + " - ");
            array[i] = scanner.nextInt();
            arraySum += array[i];
        }
        System.out.println("Сумма элментов массива равна " + arraySum);
    }
}
