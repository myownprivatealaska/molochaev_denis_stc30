import java.util.Scanner;
import java.util.Arrays;

class Program4 {
    public static void main(String[] args) {
        System.out.print("Размер: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];//по-хорошему надо бы проверить на не нуль, будем помнить, делать не буду
        int indexOfMin = 0;
        int indexOfMax = 0;

        System.out.println();
        for (int i=0; i < array.length; i++) {
            System.out.print("Введите элемент номр " + i + " - ");
            array[i] = scanner.nextInt();
        }

        System.out.println();
        System.out.println("Исходный массив " + Arrays.toString(array));

        int maxVal = array[0];
        int minVal = array[0];

        for (int i = 1; i < array.length; i++) {
            if (minVal > array[i]) {
                minVal = array[i];
                indexOfMin = i;
            }
            if (maxVal < array[i]) {
                maxVal = array[i];
                indexOfMax = i;
            }
        }

        array[indexOfMin] = maxVal;
        array[indexOfMax] = minVal;
        System.out.println("Модифицированный массив " + Arrays.toString(array));
    }
}
