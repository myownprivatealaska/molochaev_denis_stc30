class Program6 {
    public static void main(String[] args) {
        int array[] = {4, 2, 310, 3, 2, 10};
        long number = 0l;
        int power = 1;
        int indexRunner = array.length - 1;
        //Можно выводить стрингом, конкатинируя - не интересно
        //Алгоритм такой: для итога берем лонг, на случай больших до адекватности массивов
        //Итерируем с конца, повышая позицию множителем в каждом проходе
        //Для двузначных чисел делаем свой воркэраунд
        while(indexRunner >= 0) {
            if (array[indexRunner] > 9) {
                int i;
                for (i = 1; i <= array[indexRunner];) {
                    power *= 10;
                    i*=10;
                }
                number += (long)array[indexRunner]*(power/i);
            }
            else {
                number += (long) array[indexRunner] * power;
                power *= 10;
            }
            indexRunner--;
        }
        System.out.println(number);
    }
}
