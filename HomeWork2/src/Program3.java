import java.util.Scanner;

class Program3 {
    public static void main(String[] args) {
        System.out.print("Размер ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];//по-хорошему надо бы проверить на не нуль, будем помнить, делать не буду
        int arrayAverage = 0;

        for (int i=0; i<array.length; i++) {
            System.out.print("Элемент " + i + " ");
            array[i] = scanner.nextInt();
            //сперва хотел воткунть сюда arrayAverage += array[i]/n - но понял, что потери из-за дробей с увеличением размерности растут
            //в итоге тут инкрементирую, а разделю за циклом
            arrayAverage += array[i];
        }
        arrayAverage /= n;
        System.out.println();
        System.out.println("Среднее ариф-ое массива " + arrayAverage);
    }
}
