import java.util.Arrays;

class Program5 {
    public static void main(String[] args) {
        int[] array = {4, 5, 2, 3, 1, 22};
        int pooledVal = 0;
        System.out.println("Исходный массив - " + Arrays.toString(array));
        for (int i = array.length - 1; i > 0 ; i--) {
            for (int j = 0; j < i; j ++) {
                if (array[j] < array[j+1]) {
                    pooledVal = array[j];
                    array[j] = array [j+1];
                    array[j+1]=pooledVal;
                }
            }
            System.out.println("Массив в " + (array.length - i) + "-й итерации имеет вид: "+ Arrays.toString(array));
        }
        System.out.println("Итог: "+ Arrays.toString(array));
    }
}
