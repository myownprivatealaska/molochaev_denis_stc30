class Program7 {
    public static void main(String[] args) {
        int m[][] = new int[4][0];
        int height = m.length;
        int width = m[0].length;
        int iter = 1;
        int countH = 0;
        int countW = 0;
        int stopper = height*width;

        while (stopper >= iter) {
            for (int j = countW; j < width - countW; j++) {
                if (stopper < iter) break;
                m[countH][j] = iter;
                iter++;
            }
            for (int i = countH + 1; i < height - countH; i++) {
                if (stopper < iter) break;
                m[i][width - countW - 1] = iter;
                iter++;
            }
            for (int j = width - countW - 2; j > countW; j--) {
                if (stopper < iter) break;
                m[height - countH - 1][j] = iter;
                iter++;
            }
            for (int i = height - countH - 1; i > countH; i--) {
                if (stopper < iter) break;
                m[i][countW] = iter;
                iter++;
            }
            countH++;
            countW++;
        }

        for (int i=0; i<m.length; i++) {
            for (int j=0; j<m[i].length; j++) {
                System.out.print(m[i][j]+ " ");
            }
            System.out.println();
        }
    }
}
