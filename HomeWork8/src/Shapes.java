//Геометрические фигуры
public abstract class Shapes implements Relocatable, Scalable {
    public double edgeA;
    public double edgeB;
    public abstract double perimeter();
    public abstract double area();
    public String style;

    public int xPosition;
    public int yPosition;

    public Shapes(double edgeA, double edgeB) {
        this.edgeA = edgeA;
        this.edgeB = edgeB;
        this.xPosition=0;
        this.yPosition=0;
    }
    @Override
    public void offset() {
        System.out.println("X: " + this.xPosition + ", Y:" + this.yPosition);;
    }

    @Override
    public void relocate(int x, int y) {
        this.xPosition=x;
        this.yPosition=y;
        System.out.print("Координаты центра были изменены, новые координаты ");
        this.offset();
    }

    @Override
    public void scale(double rate) {
        this.edgeA *= rate;
        this.edgeB *= rate;
    }

    public String getStyle() {
        //Родительный падеж саппортинг для окончания на "ь"
        if (style.endsWith("ь")) return style.substring(0, style.length()- 1) + "и";
        //Родительный падеж саппортинг=)
        return style+"а";
    }

}
