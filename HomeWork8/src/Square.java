public class Square extends Rectangle {
    public Square(double oneSide) {
        super(oneSide, oneSide);
        this.style = "Квадрат";
    }
}
