public interface Relocatable {
    public void offset();
    public void relocate(int x, int y);
}
