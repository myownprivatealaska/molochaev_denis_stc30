public class Ellipse extends Shapes {
    public static final double PI = 3.1415;

    public Ellipse(double edgeA, double edgeB) {
        super(edgeA, edgeB);
        this.style = "Эллипс";
    }

    @Override
    public double perimeter() {
        return (this.edgeA + this.edgeB) * PI;
    }

    @Override
    public double area() {
        return this.edgeA * this.edgeB * PI;
    }


}
