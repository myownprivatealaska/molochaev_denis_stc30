public class Main {

    public static void main(String[] args) {
        //Инициализируем объекты классов-наследников геометрических фигур
        Ellipse ellipse = new Ellipse(3,4);
        Cirle cirle = new Cirle(5);
        Rectangle rectangle = new Rectangle(5,6);
        Square square = new Square(5);
        Shapes[] shapes = {ellipse, cirle, rectangle, square};
        //Площади
        for (int i=0; i<shapes.length; i++) {
            System.out.println("Периметр " + shapes[i].getStyle() + ": " + shapes[i].perimeter());
            System.out.println("Площадь " + shapes[i].getStyle() + ": " + shapes[i].area());
        }
        System.out.println();

        //Вторая часть задания на интерфейсы
        Square square1 = new Square(8);
        square1.area();
        square1.offset();
        square1.relocate(-1, 20);
        square1.scale(2);
        System.out.println("Периметр " + square1.getStyle() + ": " + square1.perimeter());
        System.out.println("Площадь " + square1.getStyle() + ": " + square1.area());
    }
}
