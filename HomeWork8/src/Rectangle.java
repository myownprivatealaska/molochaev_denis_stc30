public class Rectangle extends Shapes {
    public Rectangle(double edgeA, double edgeB) {
        super(edgeA, edgeB);
        this.style = "Прямоугольник";
    }

    @Override
    public double perimeter() {
        return (this.edgeA + this.edgeB)*2;
    }

    @Override
    public double area() {
        return this.edgeA * this.edgeB;
    }
}
