public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;
    private int count;

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator<T> {
        Node<T> current =  first;
        Node<T> returned =  null;

        @Override
        public T next() {
            if (current != null && this.hasNext()) {
                returned = current;
                current = current.next;
                return returned.value;
            }
            return current.value;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }
    }

    @Override
    public T get(int index) {
        if (index > 0 && index < count && first != null) {
            int i = 0;
            Node<T> current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Этого элемента нет!");
        return null;
    }

    @Override
    public int indexOf(T element) {
        int i = 0;
        Node<T> current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current != null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index > 1 && index <= this.size()) {
            Node<T> current = first;
            Node<T> previous = first;
            int i = 1;
            while (index > i) {
                previous = current;
                current = current.next;
                i++;
            }
            previous.next = current.next;
            count--;
        } else if (index == 1) {
            first = first.next;
            count--;
        } else System.err.println("Такого индекса нет!");
    }

    @Override
    public void insert(T element, int index) {
        if (index > 0 && index <= this.size()) {
            Node<T> newNode = new Node<>(element);
            if (index == 1) {
                newNode.next = first;
                first = newNode;
            } else if (index == this.size()) {
                last.next = newNode;
                last = newNode;
            } else {
                Node<T> current = first;
                for (int i = 1; i < index; i++) {
                    current = current.next;
                }
                newNode.next = current.next;
                current.next = newNode;
            }
            count++;
        } else System.out.println("Такого индекса нет!");
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public boolean contains(T element) {
        Node<T> current = first;
        while (current != null) {
            if (current.value == element) return true;
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(T element) {
        Node<T> current = first;
        Node<T> previous = first;
        boolean found = false;
        for (int i = 1; i < this.size(); i++) {
            if (current.value == element) {
                previous.next = current.next;
                found = true;
                break;
            }
            previous = current;
            current = current.next;
        }
        if (found) count--;
        else System.err.println("Такого элемента нет!");
    }

    public void revers() {
        if (size() == 1) System.out.println("Готово!");
        Node<T> latest = first;
        for (int i = 1; i < size(); i++) {
            Node<T> challenger = first.next;
            first.next = last.next;
            last.next = first;
            first = challenger;
        }
        first = last;
        last = latest;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}