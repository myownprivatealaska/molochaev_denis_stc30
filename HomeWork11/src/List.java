public interface List<T> extends Collection<T> {
    T get(int index);
    int indexOf(T element);
    void removeByIndex(int index);
    void insert(T element, int index);
}