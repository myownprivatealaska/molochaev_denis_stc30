import java.util.Arrays;

public class ArrayList<T> implements List<T> {
    private static final int MAX_ELEMENT = 10;
    private T[] data;
    private int count;

    public ArrayList() {
        this.data = (T[])new Object[MAX_ELEMENT];
    }

    private class ArrayListIterator implements Iterator<T> {
        private int current = 0;

        @Override
        public T next() {
            T value =  data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public T get(int index) {
        return index < count ? data[index] : null;
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) return i;
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index != -1) {
            for (int i = index; i < count - 1; i++) {
                data[i] = data[i + 1];
            }
            System.arraycopy(data, index + 1, data, index, size() - index - 1);
            this.data[--count] = null;
        } else {
            System.err.println("Элемент отсутствует в списке");
        }
    }

    @Override
    public void insert(T element, int index) {
        if (index > 0 && index < data.length) {
            if ((size() + 1) == data.length) {
                resize();
            }
            T temp = data[index];
            for (int i = index; i <= count; i++) {
                data[i] = element;
                element = data[i + 1];
                data[i + 1] = temp;
            }
            count++;
        }
        System.out.println("Индекс за пределы списка!");
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) resize();
        data[count] = element;
        count++;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    public void resize() {
        int oldCapacity = data.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        this.data = Arrays.copyOf(this.data, newCapacity);
    }


    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(T element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
}
