public class RemoteController {
    public Tv tv;

    //пульт без ТВ не бывает, надо связать
    public RemoteController(Tv tv) {
        this.tv = tv;
    }

    //Фактическ кнопка включения, на основе ранее проинициализированного экземпляра БД
    public void turnOnTheTv() {
        this.tv.setTvIsTurnOn(true);
    }

    //Выполняем условие №1 ДЗ
    public void randomProgram(int channelNumber) {
        this.tv.watchRandomProgramByChannel(channelNumber);
    }
    //Технологически-информационный метод
    public String getAboutTv() {
        return "Этот пульт связан с телевизором " + tv.getTvModel() + " в котором есть " + tv.getTvChannelCount() + " каналов!";
    }
    public void showAllTvChannels() {
        this.tv.showAllChannel();
        System.out.println();
    }
}
