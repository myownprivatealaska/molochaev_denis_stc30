public class Tv {
    private final int MAX_CHANNEL_COUNT = 16;
    private boolean isTvTurnOn;
    private String tvModel;
    private RemoteController remoteController;
    private TvChannel tvChannel[];
    private int tvChannelCount;

    //Именуем объкт названием модели
    public String getTvModel() {
        return tvModel;
    }

    //Геттер для приватного поля количества каналов
    public int getTvChannelCount() {
        return tvChannelCount;
    }
    //Конструктор класса по имени модели, максимум каналов - по свойству
    public Tv(String tvModel) {
        this.isTvTurnOn = false;
        this.tvModel = tvModel;
        this.tvChannel = new TvChannel[MAX_CHANNEL_COUNT];
    }
    //Заполняем спсиок канало на основе массива объектов
    public void fillTvChannel(TvChannel ... tvChannels) {
        //Оборачиваем проверками на оверфлоу заполнения каналов
        if (tvChannelCount + tvChannels.length <= MAX_CHANNEL_COUNT) {
            for (int i = 0; i < tvChannels.length; i++) {
                this.tvChannel[i] = tvChannels[i];
                tvChannelCount++;
            }
        }
    }
    //Связываем пульт с телевизором
    public void associateWithRemote(RemoteController remoteController) {
        this.remoteController = remoteController;
    }
    //Выключатель
    public void setTvIsTurnOn(boolean switcher) {
        this.isTvTurnOn = switcher;
    }
    //Псевдорандомайзер передач на основе номера канала
    public void watchRandomProgramByChannel(int channelNumber){
        if (this.isTvTurnOn) {
            TvChannel tvChannel = this.tvChannel[channelOverflowFilter(channelNumber) - 1];
            System.out.println("Вы смотрите случайную передачу канала: " + tvChannel.getBrandName());
            System.out.println("Название передачи: " + tvChannel.getRandomProgram());
        } else System.out.println("Телевизор следует включить воспользовавшись кнопкой turnOnTheTv()");
    }

    //Заколцьцуем каналы на случай неадекватного поведения и всяких NPE
    public int channelOverflowFilter(int channelNumber) {
        int realChannel = channelNumber;
        for (int i = 1; channelNumber > i * tvChannelCount; i++ ) {
            realChannel = channelNumber - (i * tvChannelCount);
            if (realChannel < tvChannelCount) break;
        }
        return realChannel;
    }

    public void showAllChannel() {
        System.out.print("Список каналов телевизора \"" + getTvModel() + "\": ");
        for (int i=0; i < tvChannelCount -1 ; i++) {
            System.out.print(tvChannel[i].getBrandName() + ", ");
        }
        System.out.println(tvChannel[tvChannelCount - 1].getBrandName() + "!");
    }
}
