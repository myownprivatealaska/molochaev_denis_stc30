public class Main {

    public static void main(String[] args) {
        System.out.println("Моделируемая область - ТВ.\n1. Создаем телевизор и пуль дистанционного управления: ");
        Tv samsung_smart_tv = new Tv("Samsung Smart TV");
        RemoteController controller = new RemoteController(samsung_smart_tv);
        samsung_smart_tv.associateWithRemote(controller);
        System.out.println(controller.getAboutTv());
        System.out.println("--------------------------------------------------------");

        System.out.println("2. Создаем каналы со списком программ: ");
        TvChannel SpasTV = new TvChannel("Телеканал Спас",
                new String[]{"Слово пастыря", "Беседы", "Белые ночи", "День пастыря", "Идущие к ..."});
        TvChannel StarTv = new TvChannel("Телеканал Звезда",
                new String[]{"Военная тайна", "Новости", "Десять фоторафий", "Война и мир - Захара Прилепина", "Служу России!"});
        TvChannel FirstChannel = new TvChannel("Первый канал",
                new String[]{"Пусть говорят", "Вечерний ургант", "Давай поженимся", "Модный приговор", "Голос: Дети"});
        TvChannel CultureTv = new TvChannel("Телеканал культура",
                new String[]{"Балет", "Красивая планета", "Поговорим о культуре", "Белый студия", "Жизнь замечательных идей"});
        TvChannel RenTv = new TvChannel("Телеканал Рен",
                new String[]{"Загадки человечества", "Самые шокирующие гипотезы", "Загадки человечества", "Документальный проект", "Засекреченные списки"});
        System.out.println("--------------------------------------------------------");

        System.out.println("3. *Помещаем* каналы в ранее созданный телевизор: ");
        samsung_smart_tv.fillTvChannel(SpasTV, StarTv, FirstChannel, CultureTv, RenTv);
        System.out.println(controller.getAboutTv());
        controller.showAllTvChannels();
        System.out.println("--------------------------------------------------------");

        System.out.println("4. Включаем ТВ и смотрим случайную передачу по номеру канала: ");
        controller.turnOnTheTv();
        controller.randomProgram(65);

    }
}
