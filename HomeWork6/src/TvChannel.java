import java.util.Random;

public class TvChannel {
    private String brandName;
    private TvProgram[] tvProgram;
    private Random random = new Random();

    public TvChannel(String brandName, String[] programName) {
        this.brandName = brandName;
        this.tvProgram = new TvProgram[programName.length];
        System.out.print("Телепрограмма \"");
        for (int i=0; i < programName.length; i++) {
            this.tvProgram[i] = new TvProgram(programName[i], this);
            System.out.print(programName[i] + "\", \"");
        }
        System.out.print("успешно добавлены на канал \"" + getBrandName() + "\"");
        System.out.println();
    }
    public String getRandomProgram() {
        int randomProgramIndex = random.nextInt(this.tvProgram.length);
        if (randomProgramIndex > 0) return tvProgram[randomProgramIndex - 1].programName;
        else return tvProgram[randomProgramIndex].programName;
    }

    public String getBrandName() {
        return brandName;
    }
}
